#include "async.http.server.h"

#include <boost/bind/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <thread>
#include <vector>

#include "connection.h"

server::Response server::HandleGetHelloWorld(const server::Request &request) {
  server::Response response;
  response.set(http::field::body, "Hello world from handler!");
  response.result(http::status::ok);
  return response;
}

server::asyncHttpServer::asyncHttpServer(const std::string &address,
                                         const std::string &port,
                                         std::size_t thread_pool_size)
    : acceptor_(net::make_strand(io_context_)),
      thread_pool_size_(thread_pool_size) {
  request_router.addHandler("/hello", server::HandleGetHelloWorld);
  // request_router.addHandler("/post", HandlerGetPost);
  // request_router.addHandler("/user", HandlerGetUser);

  // Open the acceptor with the option to reuse the address (i.e. SO_REUSEADDR).
  boost::asio::ip::tcp::resolver resolver(io_context_);
  boost::asio::ip::tcp::endpoint endpoint =
      *resolver.resolve(address, port).begin();
  acceptor_.open(endpoint.protocol());
  acceptor_.set_option(net::socket_base::reuse_address(true));
  acceptor_.bind(endpoint);
  acceptor_.listen();
}

void server::asyncHttpServer::run() {

  start_accept();

  // Create a pool of threads to run all of the io_contexts.
  std::vector<boost::shared_ptr<std::thread>> threads;

  for (std::size_t i = 0; i < thread_pool_size_; ++i) {
    boost::shared_ptr<std::thread> thread(
        new std::thread(boost::bind(&net::io_context::run, &io_context_)));
    threads.push_back(thread);
  }

  // Wait for all threads in the pool to exit.
  for (std::size_t i = 0; i < threads.size(); ++i)
    threads[i]->join();
}

void server::asyncHttpServer::start_accept() {

  acceptor_.async_accept(
      net::make_strand(io_context_),
      beast::bind_front_handler(&asyncHttpServer::handle_accept,
                                shared_from_this()));
}

void server::asyncHttpServer::handle_accept(beast::error_code e,
                                            tcp::socket socket) {

  if (!e) {
    std::cout << "accepted: " << socket.remote_endpoint().address().to_string() << std::endl;
    auto c = std::make_shared<Connection>(std::move(socket), request_router);
    c->start();
  }

  start_accept();
}

void server::asyncHttpServer::handle_stop() { io_context_.stop(); }
