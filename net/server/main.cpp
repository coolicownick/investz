#include <iostream>

#include "async.http.server.h"
#include "tools.h"

/*

  Example to create your own server:
  you need to inherit from class asyncHttpServer.
  Next you must define the constructor, where you can added
  call function addHandler by request_router to added routers and callbacks to
it.

server::Response HandleGetHelloWorld(const server::Request& request);
class myserver : public server::asyncHttpServer {
public:
    explicit myserver(const std::string& address, const std::string& port,
                      std::size_t thread_pool_size = get_nprocs()) :
    server::asyncHttpServer(address, port, thread_pool_size)
    {
        std::cout << "hello from myserver!\n";
        request_router.addHandler("/hello", server::HandleGetHelloWorld);
    }
};

*/

int main(int argc, char *argv[]) {
  try {
    // Check command line arguments.
    if (argc != 3) {
      std::cerr << "Usage: server <host> <port>\n"
                << "Example:\n"
                << "    server localhost 5000 \n";
      return EXIT_FAILURE;
    }

    // Initialise the server.
    // std::size_t num_threads = boost::lexical_cast<std::size_t>(argv[3]);
    auto s = std::make_shared<server::asyncHttpServer>(argv[1], argv[2]);
    //    auto s = std::make_shared<myserver>(argv[1], argv[2]);
    // Run the server until stopped.
    s->run();
    std::cout << "server is running\n";

  } catch (std::exception const &e) {
    std::cerr << "Error: " << e.what() << std::endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
