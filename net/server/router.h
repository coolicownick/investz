#pragma once
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <map>
#include <string>
#include <iostream>
namespace beast = boost::beast;  // from <boost/beast.hpp>
namespace http = beast::http;    // from <boost/beast/http.hpp>

namespace server {

using Response = http::response<http::string_body>;
using Request = http::request<http::string_body>;

}  // namespace server

namespace router {

using namespace server;
template <typename Handler>
class Router {
 public:
  void addHandler(std::string method, Handler handler) {
    m_Handlers.emplace(method, handler);
  }

  Response processRoute(const boost::beast::string_view &method, const Request &request) {

    auto handler_it = m_Handlers.find(std::string(method));
    if (handler_it != m_Handlers.end()) {
      return (handler_it->second)(request);
    }

    Response response{http::status::bad_request, request.version()};
    response.set(http::field::server, BOOST_BEAST_VERSION_STRING);
    response.result(http::status::not_found);
    response.set(http::field::body, "Not Found");

    return response;
  }

 private:
  std::map<std::string, Handler> m_Handlers;
};

}  // namespace router
