#pragma once
#include <iostream>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace beast = boost::beast;  // from <boost/beast.hpp>
namespace http = beast::http;    // from <boost/beast/http.hpp>
namespace net = boost::asio;     // from <boost/asio.hpp>
namespace ssl = net::ssl;        // from <boost/asio/ssl.hpp>
using tcp = net::ip::tcp;        // from <boost/asio/ip/tcp.hpp>

namespace client {

using Response = http::response<http::dynamic_body>;
using Request = http::request<http::string_body>;
constexpr auto httpPort = "80";
constexpr auto httpsPort = "443";
/// HTTP(tcp) client (default port is 80)
class syncHttpClient {
 public:
  explicit syncHttpClient(net::io_context& _io_context) : io_context(_io_context){};

  client::Response sendRequest(const char* host, const char* port,
                   const char* target, const char* body = nullptr, std::ostream& out = std::cout);

  client::Response sendRequest(const char* host,const http::verb &method, const char* port,
                                 const char* target, const char* body = nullptr, std::ostream& out = std::cout);

 protected:
  Request setUpRequest(const char* host, const char* target, const char* body);
  Request setUpRequest(const char* host,const http::verb &method, const char* target, const char* body);
  net::io_context& io_context;

 private:
  void closeStream(beast::tcp_stream& stream);
};

/// HTTPS(tcp) client (default port is 443)
class syncHttpSSLClient : public syncHttpClient {
 private:
 public:
  explicit syncHttpSSLClient(net::io_context& _io_context)
      : syncHttpClient(_io_context){};
  client::Response sendRequest(const char* host, const char* port,
                   const char* target, const char* body = nullptr,
                   std::ostream& out = std::cout);
  void addHeaders(std::string field, std::string value);
private:
  std::map<std::string, std::string> headers;
  void setHeaders(client::Request& req);
  void closeStream(beast::ssl_stream<beast::tcp_stream>& stream);
};

}  // namespace client
