#include "sync.http.client.h"
#include "forms.h"
#include <iostream>

int main(int argc, char *argv[]) {
  try {
    // Check command line arguments.
    if (argc < 4 || argc > 5) {
      std::cerr
          << "Usage to create client: crawler <host> <port> <target> *<body>\n"
          << "*<body> - its optional parameter, can be empty"
          << "Example:\n"
          << "    http-client-sync-ssl www.example.com 443 /\n";
      return EXIT_FAILURE;
    }

    net::io_context io_context;
    /// may be HTTPS client:
    /// client::syncHttpSSLClient client(io_context);
    client::syncHttpClient client(io_context);
    std::cout << "sync Http client is running\n";


    forms::endPointStructs::InputData obj;
    obj.ticker = "aapl";
    obj.region = "US";
    boost::property_tree::ptree pt = obj.getJson();

    std::stringstream ss;
    boost::property_tree::write_json(ss, pt);
    std::cout << ss.str();

    if (argc == 4) {
      client.sendRequest(argv[1], argv[2], argv[3], nullptr);
    } else if (argc == 5) {
      client.sendRequest(argv[1], argv[2], argv[3], ss.str().c_str());
    }

  } catch (std::exception const &e) {
    std::cerr << "Error: " << e.what() << std::endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
