#include "sync.http.client.h"
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/error.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <iostream>
#include <string>


constexpr auto TLSVersion = ssl::context::tlsv12_client;
constexpr auto HTTPVersion = 11;


//GET req
client::Request client::syncHttpClient::setUpRequest(const char *host,
                                                     const char *target,
                                                     const char *body) {
    // Set up an HTTP GET request message
    int version = HTTPVersion;
    Request request{http::verb::get, target, version};
    request.set(http::field::host, host);
    request.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
    request.set(http::field::content_type, "application/json");
    if (body != nullptr) {
        request.body() = body;
    }
    request.prepare_payload();
    return request;
}

client::Request client::syncHttpClient::setUpRequest(const char *host,
                                                        const http::verb &method,
                                                        const char *target,
                                                        const char *body) {
  // Set up an HTTP GET request message
  int version = HTTPVersion;
  Request request{method, target, version};
  request.set(http::field::host, host);
  request.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
  request.set(http::field::content_type, "application/json");
  if (body != nullptr) {
      request.body() = body;
  }
  request.prepare_payload();
  return request;
}



void client::syncHttpClient::closeStream(beast::tcp_stream &stream) {
  // Gracefully close the socket
  beast::error_code ec;
  stream.socket().shutdown(tcp::socket::shutdown_both, ec);

  // not_connected happens sometimes
  // so don't bother reporting it.
  //
  if (ec && ec != beast::errc::not_connected)
    throw beast::system_error{ec};

  // If we get here then the connection is closed gracefully
}

client::Response client::syncHttpClient::sendRequest(const char *host, const http::verb &method,
                                                     const char *port, const char *target,
                                                     const char *body, std::ostream &out) {

  // These objects perform our I/O
  tcp::resolver resolver(io_context);
  beast::tcp_stream stream(io_context);

  // Look up the domain name
  auto const results = resolver.resolve(host, port);

  // Make the connection on the IP address we get from a lookup
  stream.connect(results);

  // Set up an HTTP any method request message
  Request req = setUpRequest(host,method, target, body);
  //std::cout << "beginreq" << req << "endreq" << std::endl;
  // Send the HTTP request to the remote host
  http::write(stream, req);

  // This buffer is used for reading and must be persisted
  beast::flat_buffer buffer;

  // Declare a container to hold the response
  Response res;

  // Receive the HTTP response
  http::read(stream, buffer, res);

  // Write the message to standard out
  //out << res << std::endl;

  closeStream(stream);
  return res;
}

client::Response client::syncHttpClient::sendRequest(const char *host,
                                                     const char *port, const char *target,
                                                     const char *body, std::ostream &out) {

    // These objects perform our I/O
    tcp::resolver resolver(io_context);
    beast::tcp_stream stream(io_context);

    // Look up the domain name
    auto const results = resolver.resolve(host, port);

    // Make the connection on the IP address we get from a lookup
    stream.connect(results);

    // Set up an HTTP GET request message
    Request req = setUpRequest(host, target, body);
    std::cout << "beginreq" << req << "endreq" << std::endl;
    // Send the HTTP request to the remote host
    http::write(stream, req);

    // This buffer is used for reading and must be persisted
    beast::flat_buffer buffer;

    // Declare a container to hold the response
    Response res;

    // Receive the HTTP response
    http::read(stream, buffer, res);

    // Write the message to standard out
    //out << res << std::endl;

    closeStream(stream);
    return res;
}
void client::syncHttpSSLClient::closeStream(
    beast::ssl_stream<beast::tcp_stream> &stream) {
  // Gracefully close the stream
  beast::error_code ec;
  stream.shutdown(ec);
  if (ec == net::error::eof) {
    // Rationale:
    // http://stackoverflow.com/questions/25587403/boost-asio-ssl-async-shutdown-always-finishes-with-an-error
    ec = {};
  }
  if (ec)
    throw beast::system_error{ec};

  // If we get here then the connection is closed gracefully
}

client::Response client::syncHttpSSLClient::sendRequest(const char *host, const char *port,
                                            const char *target,
                                            const char *body,
                                            std::ostream &out) {
  std::cout << "-->sending request=" << host << " " <<  port << " " << target;
  if (body != nullptr) {
      std::cout << " " << body;
  }
  std::cout << std::endl;

  ssl::context ctx(TLSVersion);
  // This holds the root certificate used for verification
  ctx.set_default_verify_paths();

  // Verify the remote server's certificate
  ctx.set_verify_mode(ssl::verify_peer);

  // These objects perform our I/O
  tcp::resolver resolver(io_context);
  beast::ssl_stream<beast::tcp_stream> stream(io_context, ctx);

  // Set SNI Hostname (many hosts need this to handshake successfully)
  if (!SSL_set_tlsext_host_name(stream.native_handle(), host)) {
    beast::error_code ec{static_cast<int>(::ERR_get_error()),
                         net::error::get_ssl_category()};
    throw beast::system_error{ec};
  }

  // Look up the domain name
  auto const results = resolver.resolve(host, port);

  // Make the connection on the IP address we get from a lookup
  beast::get_lowest_layer(stream).connect(results);

  // Perform the SSL handshake
  stream.handshake(ssl::stream_base::client);

  // Set up an HTTP GET request message
  Request req = setUpRequest(host, target, body);
  setHeaders(req);
//  req.insert(yahooFinance::rapidApiKey__field,
//             yahooFinance::rapidApiKey__token);

  // Send the HTTP request to the remote host
  http::write(stream, req);

  // This buffer is used for reading and must be persisted
  beast::flat_buffer buffer;

  // Declare a container to hold the response
  Response res;

  // Receive the HTTP response
  http::read(stream, buffer, res);
  // Write the message to standard out
  //out << beast::buffers_to_string(res.body().data()) << std::endl;

  closeStream(stream);

  return res;
}

void client::syncHttpSSLClient::addHeaders(std::string field, std::string value) {
    std::pair<std::string, std::string> header {field, value};
    headers.insert(std::move(header));
}

void client::syncHttpSSLClient::setHeaders(client::Request& req) {
    for (auto& header : headers) {
        req.insert(header.first, header.second);
    }
}
