#include <gtest/gtest.h>

#include <boost/bind/bind.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/strand.hpp>
#include <iostream>
//#include "../notifier.h"

using tcp = boost::asio::ip::tcp;
namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace http = boost::beast::http;    // from <boost/beast/http.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
/*
TEST(_NOTIFIER_TEST, _Assert_Start_Notifier) {

    net::io_context io_context;
    auto notifier = Notifier(io_context);
    io_context.run();
}
 */