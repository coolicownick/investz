//
// Created by nickolas on 17.11.2021.
//

#include "server.h"
#include <string>
#include <cstdlib>


int main(int argc, char* argv[])
{

    auto _host = "0.0.0.0";
    auto _port = "8000";

    auto api = std::make_shared<serverApi::Api>(_host,_port,2);
    api->run();
    return 0;
}