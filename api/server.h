//
// Created by nickolas on 16.11.2021.
//

#ifndef INVESTZ_SERVER_H
#define INVESTZ_SERVER_H
#pragma once
#include "DBcontroller/DBcontroller.h"
#include "net/server/async.http.server.h"
#include "Notifier/Notifier.h"

namespace serverApi {

    class Api : public server::asyncHttpServer {
    public:
        explicit Api(const std::string &address, const std::string &port, std::size_t thread_pool_size = get_nprocs());

        static DBcontroller dBcontroller;
        static notifications::Notifier notifier;

    };

    server::Response setUser(const server::Request &request);

    server::Response setSubscribe(const server::Request &request);

    server::Response getUser(const server::Request &request);

    server::Response getSubscribe(const server::Request &request);

    server::Response updateSubscribe(const server::Request &request);

    server::Response deleteSubscribe(const server::Request &request);

    server::Response setNews(const server::Request &request);
    server::Response setConsensus(const server::Request &request);
    server::Response setPrice(const server::Request &request);
    server::Response setPrediction(const server::Request &request);

    server::Response getNews(const server::Request &request);

    server::Response setNotification(const server::Request &request);

    server::Response getNotification(const server::Request &request);
}    // namespace serverApi
#endif //INVESTZ_SERVER_H
