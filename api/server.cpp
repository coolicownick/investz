//
// Created by nickolas on 17.11.2021.
//
#include "server.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "DBcontroller/DBcontroller.h"
#include <sys/wait.h>
#include "forms.h"

using namespace forms::settings;
namespace serverApi {


    DBcontroller Api::dBcontroller = DBcontroller();
    notifications::Notifier Api::notifier = notifications::Notifier();

    Api::Api(const std::string &address, const std::string &port, std::size_t thread_pool_size) :
            server::asyncHttpServer(address, port, thread_pool_size ? get_nprocs() : thread_pool_size) {
        request_router.addHandler("/setuser", setUser);
        request_router.addHandler("/getuser", getUser);

        request_router.addHandler("/getsubscribe", getSubscribe);
        request_router.addHandler("/setsubscribe", setSubscribe);
        request_router.addHandler("/updatesubscribe", updateSubscribe);
        request_router.addHandler("/deletesubscribe", deleteSubscribe);

        request_router.addHandler("/setnews", setNews);
        request_router.addHandler("/setprediction", setPrediction);
        request_router.addHandler("/setconsensus", setConsensus);
        request_router.addHandler("/setprice", setPrice);

        request_router.addHandler("/getnews", getNews);


        request_router.addHandler("/setnotification", setNotification);
        Api::notifier.run();
    }

    server::Response setUser(const server::Request &request) {
        if (to_string(request.method()) != "POST")
            return server::Response{http::status::method_not_allowed, request.version()};

        server::Response response{http::status::bad_request, request.version()};
        std::stringstream ss(request.body().data());
        std::cout << request << std::endl;
        std::stringstream out;
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss,pt);

        auto user = User(pt);
        std::cout << "id: " << user.id << std::endl;
        if (!Api::dBcontroller.writeUser(user)) {
            response.result(http::status::internal_server_error);
            response.body() = "cannot setup user";

            return response;
        }
        response.result(http::status::ok);
        return response;
    }

    server::Response getUser(const server::Request &request) {
        if (to_string(request.method()) != "POST")
            return server::Response{http::status::method_not_allowed, request.version()};

        server::Response response{http::status::bad_request, request.version()};
        std::stringstream ss(request.body().data());
        std::cout << request << std::endl;
        std::stringstream out;
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss, pt);
        auto userID = pt.get<unsigned int>(constants::tg_user_idField);
        auto user = Api::dBcontroller.getUser(userID);
        if (!user.id) {
            response.result(http::status::internal_server_error);
            response.body() = "cannot get user";
            return response;
        }

        pt.erase(pt.begin());
        pt = user.getJson();
        response.result(http::status::ok);
        boost::property_tree::write_json(out, pt);
        response.body() = out.str();
        return response;
    }

    server::Response setSubscribe(const server::Request &request) {
        if (to_string(request.method()) != "POST")
            return server::Response{http::status::method_not_allowed, request.version()};
        std::stringstream ss(request.body().data());
        std::cout << request << std::endl;


        std::stringstream out;
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss,pt);
        std::cout << ss.str() << std::endl;
        auto setting = Setting(pt);
        server::Response response{http::status::bad_request, request.version()};
        if (!Api::dBcontroller.writeSetting(setting)) {
            response.result(http::status::internal_server_error);
            response.body() = "cannot add subscribe";
            return response;
        }
        auto notification = Notification();
        notification.ticker = setting.ticker;
        notification.tg_user_id = setting.user_tg_id;
        notification.periodic = setting.period.periodic;
        notification.closestTime = to_time_t(boost::posix_time::second_clock::local_time());

        if (notification.periodic == forms::settings::everyDay)
            notification.closestTime += notifications::dayAddSec /2;
        if (notification.periodic == forms::settings::everyWeek)
            notification.closestTime += notifications::weekAddSec;
        if (notification.periodic == forms::settings::everyWeek)
            notification.closestTime += notifications::monthAddSec;
        notification.id = Api::dBcontroller.writeNotification(notification);
        if (notification.id == 0) {
            response.result(http::status::internal_server_error);
            response.body() = "cannot set notification";
            return response;
        }
        auto crawlerRequest = CrawlerRequest();
        crawlerRequest.ticker = notification.ticker;
        crawlerRequest.notify_id = notification.id;
        crawlerRequest.closestTime = notification.closestTime;
        crawlerRequest.periodic = notification.periodic;
        crawlerRequest.types = setting.types;

        Api::notifier.addRequest(crawlerRequest);
        Api::notifier.addNotification(notification);

        response.result(http::status::ok);
        response.body() = out.str();
        return response;
    }

    server::Response getSubscribe(const server::Request &request) {
        if (to_string(request.method()) != "POST")
            return server::Response{http::status::method_not_allowed, request.version()};

        std::stringstream ss(request.body().data());
        std::cout << request << std::endl;

        std::stringstream out;
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss, pt);

        auto userID = pt.get<unsigned int>("user_tg_id");
        auto settings = Api::dBcontroller.getSettingsByUser(userID);
        pt.erase(pt.begin());
        server::Response response{http::status::bad_request, request.version()};
        if (!settings) {
            response.result(http::status::internal_server_error);
            response.body() = "cannot get subscribe";
            return response;
        }
        boost::property_tree::ptree sets;
        for (auto set: *settings) {
            boost::property_tree::ptree row;
            row = set.getJson();
            sets.push_back(std::make_pair("", row));
        }
        pt.add_child(forms::constants::settingsField,sets);
        boost::property_tree::write_json(out, pt);
        response.body() = out.str();
        response.result(http::status::ok);
        return response;
    }

    server::Response updateSubscribe(const server::Request &request) {
        if (to_string(request.method()) != "POST")
            return server::Response{http::status::method_not_allowed, request.version()};

        std::stringstream ss(request.body().data());
        std::cout << request << std::endl;

        std::stringstream out;
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss,pt);

        auto setting = Setting(pt);
        server::Response response{http::status::bad_request, request.version()};
        if (!Api::dBcontroller.updateSetting(setting)) {
            response.result(http::status::internal_server_error);
            response.body() = "cannot update subscribe";
            return response;
        }
        response.result(http::status::ok);
        response.body() = out.str();
        return response;
    }

    server::Response deleteSubscribe(const server::Request &request) {
        if (to_string(request.method()) != "POST")
            return server::Response{http::status::method_not_allowed, request.version()};

        std::stringstream ss(request.body().data());
        std::cout << request << std::endl;

        std::stringstream out;
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss, pt);
        auto setting = forms::settings::DeleteSetting(pt);
        server::Response response{http::status::bad_request, request.version()};
        if (!Api::dBcontroller.deleteSetting(setting)) {
            response.result(http::status::internal_server_error);
            response.body() = "cannot delete subscribe";
            return response;
        }
        auto notify_id = Api::dBcontroller.deleteNotification(setting);
        if (notify_id == 0) {
            response.result(http::status::internal_server_error);
            response.body() = "cannot delete subscribe";
            return response;
        }
        Api::notifier.deleteRequestNotify(notify_id);
        pt.erase(pt.begin());
        response.result(http::status::ok);
        response.body() = out.str();
        return response;
    }

    server::Response setNews(const server::Request &request) {
        if (to_string(request.method()) != "POST")
            return server::Response{http::status::method_not_allowed, request.version()};

        std::stringstream ss(request.body().data());
        std::cout << request << std::endl;

        std::stringstream out;
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss, pt);
        auto newsJSON = pt.get_child(forms::constants::newsField);
        auto newsVector = std::make_shared<std::vector<forms::News>>();
        server::Response response{http::status::bad_request, request.version()};
        bool res = false;
        for (auto &pair: newsJSON) {
            auto news = News(pair.second);
            if (!Api::dBcontroller.writeNews(news))
                continue;
            newsVector->push_back(news);
            res = true;
        }
        if(!res){
            response.result(http::status::internal_server_error);
            response.body() = "cannot set news";
            return response;
        }
        Api::notifier.setNewsToNotify(newsVector);
        response.result(http::status::ok);
        response.body() = out.str();
        return response;
    }

    server::Response getNews(const server::Request &request) {
        if (to_string(request.method()) != "POST")
            return server::Response{http::status::method_not_allowed, request.version()};
        std::stringstream ss(request.body().data());
        std::cout << request << std::endl;

        std::stringstream out;
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss, pt);
        auto notyID = pt.get<unsigned int>("notify_id");
        auto news = Api::dBcontroller.getNews(notyID);
        server::Response response{http::status::bad_request, request.version()};
        if (!news.get()) {
            response.result(http::status::internal_server_error);
            response.body() = "there is none news";
            return response;
        }
        pt.erase(pt.begin());
        for (auto n: *news) {
            boost::property_tree::ptree row;
            row = n.getJson();
            pt.push_back(std::make_pair("", row));
        }
        delete news.get();
        boost::property_tree::ptree ptNews;
        ptNews.add_child(forms::constants::newsField, pt);

        boost::property_tree::write_json(out, ptNews);
        response.result(http::status::ok);
        response.body() = out.str();
        return response;
    }

    server::Response setConsensus(const server::Request &request) {
        if (to_string(request.method()) != "POST")
            return server::Response{http::status::method_not_allowed, request.version()};

        std::stringstream ss(request.body().data());
        std::cout << request << std::endl;

        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss, pt);
        auto consensusJSON = pt.get_child(forms::constants::consensusField);
        auto consensusesVector = std::make_shared<std::vector<forms::Consensus>>();
        server::Response response{http::status::bad_request, request.version()};
        bool res = false;
        for (auto &pair: consensusJSON) {
            auto consensus = Consensus(pair.second);
            if (!Api::dBcontroller.writeConsensus(consensus))
                continue;
            consensusesVector->push_back(consensus);
            res = true;
        }
        if(!res){
            response.result(http::status::internal_server_error);
            response.body() = "cannot set consensus";
            return response;
        }
        Api::notifier.setConsensusToNotify(consensusesVector);
        response.result(http::status::ok);
        response.body() = "ok";
        return response;
    }

    server::Response setPrice(const server::Request &request) {
        if (to_string(request.method()) != "POST")
            return server::Response{http::status::method_not_allowed, request.version()};

        std::stringstream ss(request.body().data());
        std::cout << request << std::endl;
        std::stringstream out;
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss, pt);
        auto price = std::make_shared<forms::Price>(pt.get_child(forms::constants::priceField));
        server::Response response{http::status::bad_request, request.version()};
        if (!Api::dBcontroller.writePrice(*price)) {
                response.result(http::status::internal_server_error);
                response.body() = "cannot set price";
                return response;
        }
        Api::notifier.setPriceToNotify(price);
        response.result(http::status::ok);
        response.body() = out.str();
        return response;
    }

    server::Response setPrediction(const server::Request &request) {
        if (to_string(request.method()) != "POST")
            return server::Response{http::status::method_not_allowed, request.version()};

        std::stringstream ss(request.body().data());
        std::cout << request << std::endl;
        std::stringstream out;
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss, pt);
        auto prediction = std::make_shared<forms::Prediction>(pt);
        server::Response response{http::status::bad_request, request.version()};
        if (!Api::dBcontroller.writePrediction(*prediction)) {
            response.result(http::status::internal_server_error);
            response.body() = "cannot set prediction";
            return response;
        }
        Api::notifier.setPredictToNotify(prediction);

        response.result(http::status::ok);
        response.body() = out.str();
        return response;
    }

    server::Response setNotification(const server::Request &request) {
        if (to_string(request.method()) != "POST")
            return server::Response{http::status::method_not_allowed, request.version()};

        std::stringstream ss(request.body().data());
        std::cout << request << std::endl;

        std::stringstream out;
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss, pt);
        auto notification = Notification(pt);
        server::Response response{http::status::bad_request, request.version()};
        if (!Api::dBcontroller.writeNotification(notification)) {
            response.result(http::status::internal_server_error);
            response.body() = "cannot set notification";
            return response;
        }
        response.result(http::status::ok);
        response.body() = out.str();
        return response;
    }


} // namespace serverApi