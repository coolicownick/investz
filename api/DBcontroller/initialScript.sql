SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';

DROP TABLE  IF EXISTS
    actor,
    settings,
    actor_settings,
    notifications,
    news,
    price,
    consensus,
    prediction
    CASCADE ;

CREATE TABLE IF NOT EXISTS actor
(
    id serial primary key,
    extID bigint NOT NULL unique,
    username varchar(50) NOT NULL unique,
    firstname varchar(50),
    lastname varchar(50)
    );

ALTER TABLE actor OWNER TO investz;

CREATE TABLE IF NOT EXISTS settings
(
    id serial primary key,
    ticker varchar(10) NOT NULL,
    periodType smallint NOT NULL,
    news boolean,
    consensus boolean,
    price boolean,
    prediction boolean,
    summaryType smallint NOT NULL,
    UNIQUE (ticker,periodType,news,consensus,price,prediction,summaryType)
    );

ALTER TABLE settings OWNER TO investz;

CREATE TABLE IF NOT EXISTS actor_settings
(
    actor_id integer NOT NULL DEFAULT nextval('actor_id_seq'::regclass),
    settings_id integer NOT NULL DEFAULT nextval('actor_id_seq'::regclass),
    FOREIGN KEY (actor_id) REFERENCES actor (id) MATCH SIMPLE,
    FOREIGN KEY (settings_id) REFERENCES settings (id) MATCH SIMPLE
    );

ALTER TABLE actor_settings OWNER TO investz;

CREATE TABLE IF NOT EXISTS notifications
(
    id serial primary key ,
    ticker varchar(10) NOT NULL,
    actor_id integer,
    user_tg_id integer NOT NULL,
    periodType smallint NOT NULL,
    FOREIGN KEY (actor_id) REFERENCES actor (id) MATCH SIMPLE ON DELETE CASCADE,
    UNIQUE(user_tg_id,ticker)
    );

ALTER TABLE notifications OWNER TO investz;

CREATE TABLE IF NOT EXISTS news
(
    id serial primary key,
    notify_id integer,
    ticker varchar(10)  NOT NULL,
    title varchar(400) NOT NULL,
    body varchar,
    imagelink varchar(500),
    createdDate bigint NOT NULL,
    author varchar(50),
    link varchar(500) NOT NULL,
    FOREIGN KEY (notify_id) REFERENCES notifications (id) MATCH SIMPLE ON DELETE CASCADE,
    UNIQUE (notify_id,ticker,title,link)
    );

ALTER TABLE news OWNER TO investz;

CREATE TABLE IF NOT EXISTS price
(
    id serial primary key,
    notify_id integer,
    ticker varchar(10) NOT NULL,
    price numeric(10,6) NOT NULL,
    createdDate bigint NOT NULL,
    FOREIGN KEY (notify_id) REFERENCES notifications (id) MATCH SIMPLE ON DELETE CASCADE,
    UNIQUE (notify_id,ticker,price,createdDate)
    );

ALTER TABLE price OWNER TO investz;


CREATE TABLE IF NOT EXISTS consensus
(
    id serial primary key,
    notify_id integer,
    ticker varchar(10) ,
    title varchar(400),
    body varchar,
    recommendation varchar(10) ,
    createdDate bigint NOT NULL,
    author varchar(40),
    FOREIGN KEY (notify_id) REFERENCES notifications (id) MATCH SIMPLE ON DELETE CASCADE,
    UNIQUE (notify_id,ticker,title)
    );

ALTER TABLE consensus OWNER TO investz;

CREATE TABLE IF NOT EXISTS prediction
(
    id serial primary key,
    notify_id integer,
    ticker varchar(10) NOT NULL,
    target numeric(10,6) NOT NULL,
    createdDate bigint NOT NULL,
    FOREIGN KEY (notify_id) REFERENCES notifications (id) MATCH SIMPLE ON DELETE CASCADE,
    UNIQUE (notify_id,ticker,target,createdDate)
    );
ALTER TABLE prediction OWNER TO investz;




