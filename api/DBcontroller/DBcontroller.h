//
// Created by nickolas on 18.11.2021.
//

#ifndef INVESTZ_DBCONTROLLER_H
#define INVESTZ_DBCONTROLLER_H
#pragma once

#include <string>

#include "forms.h"
#include <pqxx/pqxx>


#include "forms.h"
using namespace forms;

class DBcontroller{
public:
    DBcontroller();
    // user entity
    bool writeUser(const User &user);
    User getUser(const unsigned int userTGID);
    // settings entity
    bool writeSetting(const settings::Setting &setting);
    std::shared_ptr<std::vector<settings::Setting>> getSettingsByUser(const unsigned int userTGID);
    bool updateSetting(const settings::Setting &setting);
    bool deleteSetting(const settings::DeleteSetting &delSetting);
    bool writeNews(const News &news);
    std::shared_ptr<std::vector<News>> getNews(const unsigned int notyID);
    bool writeConsensus(const Consensus &consensus);
    std::shared_ptr<std::vector<Consensus>> getConsensus(const unsigned int notyID);
    bool writePrice(const Price &price);
    std::shared_ptr<std::vector<Price>> getPrice(const unsigned int notyID);
    bool writePrediction(const Prediction &prediction);
    std::shared_ptr<std::vector<Prediction>> getPrediction(const unsigned int notyID);

    unsigned int writeNotification(const Notification &notification);
    unsigned int deleteNotification(const settings::DeleteSetting &deleteSetting);
private:
    pqxx::connection connect;
    unsigned int checkUser(pqxx::work &tx, const unsigned int userTGID);
    unsigned int checkSetting(pqxx::work &tx, const settings::Setting &setting);
    bool checkActorSetting(pqxx::work &tx, const unsigned int userID, const unsigned int settingID);
    //tickerInfo<Risk> Risks;
};
#endif //INVESTZ_DBCONTROLLER_H
