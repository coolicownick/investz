//
// Created by nickolas on 30.11.2021.
//

#include "DBcontroller.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <pqxx/pqxx>

DBcontroller::DBcontroller() :
        connect(std::getenv("CONNECT_STRING")) {

    if (connect.is_open()) {
        std::cout << "Opened database successfully: " << connect.dbname() << std::endl;
    } else {
        std::cout << "Can't open database" << std::endl;
        exit(1);
    }

    pqxx::work tx{connect};
    std::ifstream file("initialScript.sql");
    if (!file) {
        throw std::runtime_error("Cannot generate controller. None .sql file");
    }
    std::stringstream buffer;
    buffer << file.rdbuf();
    file.close();
    tx.exec(buffer.str());
    tx.commit();

    {// actor prepared queries
        connect.prepare("add actor", "insert into actor (extID,username,firstname,lastname) values($1,$2,$3,$4)");
        connect.prepare("find actor", "select * from actor where extID=$1");
    }
    {// actor_setting prepared queries
        connect.prepare("check actor setting", "select * from actor_settings where actor_id=$1 and settings_id = $2");
        connect.prepare("check actor setting count", "select * from actor_settings where settings_id = $1");
        connect.prepare("find actor setting id",
                        "select * from settings as s left join actor_settings as act_set "
                        "on s.id = act_set.settings_id where act_set.actor_id = $1 and s.ticker = $2");
    }
    {// setting prepared queries
        connect.prepare("check setting", "select * from settings where ticker=$1 and periodType=$2 and"
                                         " news=$3 and consensus=$4 and price=$5 and prediction=$6 and summaryType=$7");

        connect.prepare("add setting", "insert into settings (ticker, periodtype, news,"
                                       " consensus, price, prediction, summarytype) values($1,$2,$3,$4,$5,$6,$7) returning id");
        connect.prepare("set actor_setting", "insert into actor_settings (actor_id,settings_id) values($1,$2)");
        connect.prepare("find settings", "select * from settings as s left join actor_settings as rel"
                                         " on s.id = rel.settings_id where rel.actor_id = $1");
        connect.prepare("update setting", "update settings set ticker=$1, periodtype=$2, news=$3,"
                                          " consensus=$4, price=$5, prediction=$6, summarytype=$7 where id=$8");
        connect.prepare("delete setting", "delete from settings where id=$1");
        connect.prepare("delete actor_setting",
                        "delete from actor_settings where actor_id = $1 and settings_id = $2");
    }
    {// news prepared queries
        connect.prepare("add news",
                        "insert into news (notify_id,ticker,title,body,imagelink,createddate,author,link) values($1,$2,$3,$4,$5,$6,$7,$8)");
        connect.prepare("get news by id", "select * from news where notify_id=$1");
        connect.prepare("get news by ticker&date", "select * from news where ticker=$1 and createddate in ($2,$3)");

    }

    {// consensus prepared queries
        connect.prepare("add consensus",
                        "insert into consensus (notify_id,ticker,title,body,recommendation, createddate,author) values($1,$2,$3,$4,$5,$6,$7)");
        connect.prepare("get consensus by id", "select * from consensus where notify_id=$1");
        connect.prepare("get consensus by ticker&date",
                        "select * from consensus where ticker=$1 and createddate in ($2,$3)");

    }
    {// news prepared queries
        connect.prepare("add price",
                        "insert into price (notify_id,ticker,price,createddate) values($1,$2,$3,$4)");
        connect.prepare("get price by id", "select * from price where notify_id=$1");
        connect.prepare("get price by ticker&date", "select * from price where ticker=$1 and createddate in ($2,$3)");

    }
    {// news prepared queries
        connect.prepare("add prediction",
                        "insert into prediction (notify_id,ticker,target,createddate) values($1,$2,$3,extract(epoch from now())*1000)");
        connect.prepare("get prediction by id", "select * from prediction where notify_id=$1");
        connect.prepare("get prediction by ticker&date",
                        "select * from prediction where ticker=$1 and createddate in ($2,$3)");

    }
    {// notification prepared queries
        connect.prepare("add notifications",
                        "insert into notifications (ticker,actor_id,user_tg_id,periodType) values($1,$2,$3,$4) returning id");
        connect.prepare("get notifications by id", "select * from notifications where id=$1");
        connect.prepare("delete notification by tg & ticker",
                        "delete from notifications where user_tg_id=$1 and ticker=$2 returning id");

    }
}


bool DBcontroller::writeUser(const User &user) {
    pqxx::work tx{connect};
    try {
        auto userID = checkUser(tx, user.id);
        if (userID > 0) {
            tx.abort();
            return false;
        }
        tx.exec_prepared("add actor", user.id, user.username, user.firstName, user.lastName);
        tx.commit();
        return true;
    } catch (const std::exception &e) {
        tx.abort();
        return false;
    }
}

User DBcontroller::getUser(const unsigned int userTGID) {
    pqxx::work tx{connect};
    auto user = User();
    try {
        pqxx::result r{tx.exec_prepared("find actor", userTGID)};
        if (r.empty()) {
            tx.abort();
            return user;
        }
        user.id = r.at(0)["extID"].as<int>();
        user.username = r.at(0)["username"].as<std::string>();
        user.firstName = r.at(0)["firstname"].as<std::string>();
        user.lastName = r.at(0)["lastname"].as<std::string>();
        tx.commit();
    } catch (const std::exception &e) {
        std::cerr << "get user transaction: " << e.what() << std::endl;
        tx.abort();
        return user;
    }
    return user;
}

bool DBcontroller::writeSetting(const settings::Setting &setting) {
    pqxx::work tx{connect};
    try {
        auto settingID = checkSetting(tx, setting);
        if (settingID == 0) {

            pqxx::result res{tx.exec_prepared("add setting", setting.ticker, setting.period.periodic,
                                              setting.types.news, setting.types.consensus, setting.types.price,
                                              setting.types.prediction,
                                              setting.summary.periodic)};
            if (res.empty()) {
                tx.abort();
                return false;
            }
            settingID = res.at(0)["id"].as<unsigned int>();
        }
        if (checkActorSetting(tx, setting.user_tg_id, settingID)) {
            tx.abort();
            return false;
        }
        pqxx::result r{tx.exec_prepared("find actor", setting.user_tg_id)};
        if (r.empty()) {
            tx.abort();
            return false;
        }
        auto actorID = r.at(0)["id"].as<unsigned int>();

        tx.exec_prepared("set actor_setting", actorID, settingID);
        tx.commit();
        return true;
    } catch (const std::exception &e) {
        std::cerr << "set settings transaction: " << e.what() << std::endl;
        tx.abort();
        return false;
    }
}


std::shared_ptr <std::vector<settings::Setting>> DBcontroller::getSettingsByUser(const unsigned int userTGID) {
    pqxx::work tx{connect};

    try {
        auto settings = std::make_shared < std::vector < settings::Setting >> ();
        auto userID = checkUser(tx, userTGID);
        if (userID == 0)
            return settings;
        pqxx::result res{tx.exec_prepared("find settings", userID)};
        for (auto const &row: res) {
            auto set = settings::Setting();
            set.id = row["id"].as<unsigned int>();
            set.ticker = row["ticker"].as<std::string>();
            std::cout << "ticker: " << set.ticker << std::endl;
            set.user_tg_id = userID;
            set.period.periodic = row["periodtype"].as<unsigned int>();
            set.summary.periodic = row["summarytype"].as<unsigned int>();
            set.types.news = row["news"].as<bool>();
            set.types.consensus = row["consensus"].as<bool>();
            set.types.price = row["price"].as<bool>();
            set.types.prediction = row["prediction"].as<bool>();
            settings->push_back(set);
        }
        tx.commit();
        return settings;
    } catch (const std::exception &e) {
        std::cerr << "get settings transaction: " << e.what() << std::endl;
        tx.abort();
        return nullptr;
    }
}

bool DBcontroller::updateSetting(const settings::Setting &setting) {

    pqxx::work tx{connect};
    try {
        auto checkedSetting = checkSetting(tx, setting);
        if (checkedSetting == 0) {
            tx.exec_prepared("update setting", setting.ticker, setting.period.periodic,
                             setting.types.news, setting.types.consensus, setting.types.price,
                             setting.types.prediction, setting.summary.periodic, setting.id);
            tx.commit();
            return true;

        }
        std::cout << "checke set: " << checkedSetting << std::endl;
        pqxx::result r{tx.exec_prepared("find actor", setting.user_tg_id)};
        if (r.empty()) {
            tx.abort();
            return false;
        }
        auto actorID = r.at(0)["id"].as<unsigned int>();
        tx.exec_prepared("set actor_setting", actorID, checkedSetting);
        tx.commit();
        auto delSetting = forms::settings::DeleteSetting();
        delSetting.ticker = setting.ticker;
        delSetting.user_tg_id = setting.user_tg_id;
        return deleteSetting(delSetting);
    } catch (const std::exception &e) {
        std::cerr << "update settings transaction: " << e.what() << std::endl;
        tx.abort();
        return false;
    }
}


bool DBcontroller::deleteSetting(const settings::DeleteSetting &delSetting) {
    pqxx::work tx{connect};

    try {
        auto actorID = checkUser(tx, delSetting.user_tg_id);
        pqxx::result r{tx.exec_prepared("find actor setting id", actorID, delSetting.ticker)};
        if (r.empty()) {
            tx.abort();
            std::cout << "debug actorID: " << actorID << " ticker: " << delSetting.ticker << std::endl;
            return false;
        }
        auto settingID = r.at(0)["settings_id"].as<unsigned int>();
        tx.exec_prepared("delete actor_setting", actorID, settingID);
        pqxx::result resCount{tx.exec_prepared("check actor setting count", settingID)};
        if (resCount.empty())
            tx.exec_prepared("delete setting", settingID);
        tx.commit();
        return true;
    } catch (const std::exception &e) {
        std::cerr << "delete settings transaction: " << e.what() << std::endl;
        tx.abort();
        return false;
    }

}

unsigned int DBcontroller::checkUser(pqxx::work &tx, const unsigned int userTGID) {

    pqxx::result res{tx.exec_prepared("find actor", userTGID)};
    if (res.empty())
        return 0;
    return res.at(0)["id"].as<unsigned int>();
}

unsigned int DBcontroller::checkSetting(pqxx::work &tx, const settings::Setting &setting) {
    pqxx::result res{tx.exec_prepared("check setting", setting.ticker, setting.period.periodic,
                                      setting.types.news, setting.types.consensus, setting.types.price,
                                      setting.types.prediction, setting.summary.periodic)};
    if (res.empty())
        return 0;
    return res.at(0)["id"].as<unsigned int>();
}

bool DBcontroller::checkActorSetting(pqxx::work &tx, const unsigned int userID, const unsigned int settingID) {
    pqxx::result res{tx.exec_prepared("check actor setting", userID, settingID)};
    if (res.empty())
        return false;
    return true;
}

bool DBcontroller::writeNews(const News &news) {
    pqxx::work tx{connect};
    try {

        tx.exec_prepared("add news", news.notify_id, news.ticker, news.title, news.body, news.image,
                         news.publishedDate, news.author, news.source);
        tx.commit();
        return true;
    } catch (const std::exception &e) {
        std::cerr << "write news transaction: " << e.what() << std::endl;
        tx.abort();
        return false;
    }
}

bool DBcontroller::writeConsensus(const Consensus &consensus) {
    pqxx::work tx{connect};
    try {
        tx.exec_prepared("add consensus", consensus.notify_id, consensus.ticker, consensus.title, consensus.body,
                         consensus.recommendation, consensus.publishedDate, consensus.author);
        tx.commit();
        return true;
    } catch (const std::exception &e) {
        std::cerr << "write consensus transaction: " << e.what() << std::endl;
        tx.abort();
        return false;
    }
}

bool DBcontroller::writePrice(const Price &price) {
    pqxx::work tx{connect};
    try {

        tx.exec_prepared("add price", price.notify_id, price.ticker, price.price, price.date);
        tx.commit();
        return true;
    } catch (const std::exception &e) {
        std::cerr << "write price transaction: " << e.what() << std::endl;
        tx.abort();
        return false;
    }
}

bool DBcontroller::writePrediction(const Prediction &prediction) {
    pqxx::work tx{connect};
    try {

        tx.exec_prepared("add prediction", prediction.notify_id, prediction.ticker, prediction.target);
        tx.commit();
        return true;
    } catch (const std::exception &e) {
        std::cerr << "write news transaction: " << e.what() << std::endl;
        tx.abort();
        return false;
    }
}

std::shared_ptr <std::vector<News>> DBcontroller::getNews(const unsigned int notyID) {
    pqxx::work tx{connect};
    try {
        pqxx::result res{tx.exec_prepared("get news by id", notyID)};
        if (res.empty()) {
            tx.abort();
            return nullptr;
        }
        auto news = std::make_shared < std::vector < News >> ();
        for (auto n: res) {
            News newsToAdd;
            newsToAdd.id = n["id"].as<unsigned int>();
            newsToAdd.notify_id = n["notify_id"].as<unsigned int>();
            newsToAdd.title = !n["title"].is_null() ? n["title"].as<std::string>() : "";
            newsToAdd.ticker = !n["ticker"].is_null() ? n["ticker"].as<std::string>() : "";
            newsToAdd.body = !n["body"].is_null() ? n["body"].as<std::string>() : "";
            newsToAdd.image = !n["imagelink"].is_null() ? n["imagelink"].as<std::string>() : "";
            newsToAdd.publishedDate = n["createddate"].as<unsigned long>();
            newsToAdd.source = !n["link"].is_null() ? n["link"].as<std::string>() : "";
            newsToAdd.author = !n["author"].is_null() ? n["author"].as<std::string>() : "";
            news->push_back(newsToAdd);
        }
        tx.commit();
        return news;
    } catch (const std::exception &e) {
        std::cerr << "get news transaction: " << e.what() << std::endl;
        tx.abort();
        return nullptr;
    }
}


unsigned int DBcontroller::writeNotification(const Notification &notification) {
    pqxx::work tx{connect};
    try {
        pqxx::result r{tx.exec_prepared("find actor", notification.tg_user_id)};
        if (r.empty()) {
            tx.abort();
            return 0;
        }
        auto actorID = r.at(0)["id"].as<unsigned int>();

        pqxx::result res{tx.exec_prepared("add notifications", notification.ticker, actorID, notification.tg_user_id,
                                          notification.periodic)};
        if (res.empty()) {
            tx.abort();
            return 0;
        }
        auto notyID = res.at(0)["id"].as<unsigned int>();
        tx.commit();
        return notyID;
    } catch (const std::exception &e) {
        std::cerr << "write notification transaction: " << e.what() << std::endl;
        tx.abort();
        return 0;
    }
}

unsigned int DBcontroller::deleteNotification(const settings::DeleteSetting &deleteSetting) {
    pqxx::work tx{connect};
    try {
        pqxx::result r{
                tx.exec_prepared("delete notification by tg & ticker", deleteSetting.user_tg_id, deleteSetting.ticker)};
        if (r.empty()) {
            tx.abort();
            return 0;
        }
        auto notifyID = r.at(0)["id"].as<unsigned int>();
        tx.commit();
        return notifyID;
    } catch (const std::exception &e) {
        std::cerr << "write notification transaction: " << e.what() << std::endl;
        tx.abort();
        return 0;
    }
}




