#!/bin/bash

rm -rf build && mkdir build && cd build
cmake ..
make -j4
cp ../DBcontroller/initialScript.sql ./
./main 0.0.0.0 8080 &
pid="$!"

while true
do
  if [ -d /proc/$pid ]; then
    wait "$pid"
    sleep 1
  else
    make -j4
    ./main 0.0.0.0 8080 &
    pid="$!"
  fi
done
