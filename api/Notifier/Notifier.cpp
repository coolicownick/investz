//
// Created by nickolas on 04.12.2021.
//
#include "Notifier.h"
#include <boost/bind.hpp>
#include <memory>
#include <boost/property_tree/ptree.hpp>
#include <thread>

namespace notifications {

    Notifier::Notifier() :
            client::syncHttpClient(io)
    //timer(new boost::asio::deadline_timer(io))
    {
        timer = std::make_unique<boost::asio::deadline_timer>(io);
        crawlerReq = std::make_unique<multimapPotected<forms::CrawlerRequest>>();
        notifications = std::make_unique<multimapPotected<forms::Notification>>();
    }

    /*void Notifier::run(){

    }
     */
    void Notifier::run() {
        std::thread runTimer([&]() {
            checkMaps();
            io.run();
        });
        std::thread checkNotifications([&](){
            while (true) {
                sendNotifications();
            }
        });
        std::thread checkRequests([&](){
            while (true) {
                sendCrawlerRequests();
            }
        });
        runTimer.detach();
        checkRequests.detach();
        checkNotifications.detach();
    }


    void Notifier::checkMaps() {
        std::cout << "current time " << (boost::posix_time::second_clock::local_time()) << std::endl;
        mapsCheck.notify_all();
        timer->expires_from_now(boost::posix_time::seconds(10));
        timer->async_wait(boost::bind(&Notifier::checkMaps, this));
    }

     void Notifier::sendNotifications() {
             {
                 std::unique_lock<std::mutex> locker(notifications->mutex);
                 mapsCheck.wait(locker);
                 std::cout << "sending notifications..." << std::endl;
             }
             auto firstNotify = notifications->multimap.begin();
             if (firstNotify == notifications->multimap.end()) {
                 std::cout << "notifications empty " << std::endl;
                 notifications->mutex.unlock();
                 return;
             }
             auto result = firstNotify->first - to_time_t(boost::posix_time::second_clock::local_time());
             std::cout << "rest notifications time:  " << result << std::endl;
             while (firstNotify->first - to_time_t(boost::posix_time::second_clock::local_time()) < limitNotifySec) {
                 auto json = firstNotify->second.getJson();
                 std::stringstream out;
                 boost::property_tree::write_json(out, json);
                 std::cout << "notify to send: " << out.str() << std::endl;
                 auto response = sendRequest("web",http::verb::post, "80", "/bot/notification", out.str().c_str());
                 std::cout << "sending notifications.." << response.result() << std::endl;
                 if(response.result() != http::status::ok)
                     continue;
                 clearNotifyData(firstNotify->second);
                 if(!setNextNotyIfStillData(firstNotify->second))
                    incrementTime<forms::Notification>(firstNotify);
                 std::cout << "sent noty for " << firstNotify->second.ticker << std::endl;

                 notifications->multimap.insert(
                         std::pair<unsigned long, forms::Notification>(firstNotify->second.closestTime, firstNotify->second));
                 notifications->multimap.erase(firstNotify);
                 firstNotify = notifications->multimap.begin();
             }
            notifications->mutex.unlock();
    }

    void Notifier::clearNotifyData(forms::Notification &noty){
        noty.prices = forms::Price();
        noty.prediction = forms::Prediction();
        if(noty.news.size() > 0)
            noty.news.erase(noty.news.begin());
        if(noty.consensuses.size() > 0)
            noty.consensuses.erase(noty.consensuses.begin());
    }

    bool Notifier::setNextNotyIfStillData(forms::Notification &noty){
        if(noty.prices.ticker != "" || noty.prediction.ticker != ""
        || !noty.news.empty() || !noty.consensuses.empty()){
            noty.closestTime += nextNotyAddSec;
            return true;
        }
        return false;
    }

    void Notifier::sendCrawlerRequests() {
        {
            std::unique_lock<std::mutex> locker(crawlerReq->mutex);
            mapsCheck.wait(locker);
            std::cout << "sending crawler requests..." << std::endl;
        }
        while (crawlerReq->mutex.try_lock());
        auto firstReq = crawlerReq->multimap.begin();
        if (firstReq == crawlerReq->multimap.end()) {
            std::cout << "requests empty " << std::endl;
            crawlerReq->mutex.unlock();
            return;
        }
        auto result = firstReq->first - to_time_t(boost::posix_time::second_clock::local_time());
        std::cout << "rest crawler request time:  " << result << std::endl;
        while (firstReq->first - to_time_t(boost::posix_time::second_clock::local_time()) < limitCrawlerRecSec) {
            auto json = firstReq->second.getJson();
            boost::property_tree::ptree pt;
            pt.add_child("crawlerRequest",json);
            std::stringstream out;
            boost::property_tree::write_json(out, pt);
            std::cout << "crawler req: " << out.str() << std::endl;
            auto response = sendRequest("web",http::verb::post , "80", crawlerPath, out.str().c_str());
            std::cout << response.result() << std::endl;
            incrementTime<forms::CrawlerRequest>(firstReq);
            std::cout << "sent req for " << firstReq->second.ticker << std::endl;
            crawlerReq->multimap.erase(firstReq);
            crawlerReq->multimap.insert(
                    std::pair<unsigned long, forms::CrawlerRequest>(firstReq->second.closestTime, firstReq->second));
            firstReq = crawlerReq->multimap.begin();
        }
        crawlerReq->mutex.unlock();
    }
    void Notifier::addRequest(const forms::CrawlerRequest &request) {
        while (!crawlerReq->mutex.try_lock());
        crawlerReq->multimap.insert(std::pair<unsigned long, forms::CrawlerRequest>(request.closestTime, request));
        std::cout << "inserted req for " << request.ticker << std::endl;
        crawlerReq->mutex.unlock();
    }

    void Notifier::addNotification(const forms::Notification &notification) {
        while (!crawlerReq->mutex.try_lock());
        notifications->multimap.insert(
                std::pair<unsigned long, forms::Notification>(notification.closestTime, notification));
        std::cout << "inserted notification for " << notification.ticker << std::endl;
        crawlerReq->mutex.unlock();
    }

    // handle different content add
    void Notifier::setNewsToNotify(std::shared_ptr<std::vector<forms::News>> news) {
        while (!notifications->mutex.try_lock());
        for(auto it = notifications->multimap.begin(); it != notifications->multimap.end(); ++it){
            if(it->second.id == news->at(0).notify_id){
                it->second.news = *news;
                break;
            }
        }
        notifications->mutex.unlock();
    }
    void Notifier::setConsensusToNotify(std::shared_ptr<std::vector<forms::Consensus>> consensus) {
        while (!notifications->mutex.try_lock());
        for(auto it = notifications->multimap.begin(); it != notifications->multimap.end(); ++it){
            if(it->second.id == consensus->at(0).notify_id){
                it->second.consensuses = *consensus;
                break;
            }
        }
        notifications->mutex.unlock();
    }
    void Notifier::setPriceToNotify(std::shared_ptr<forms::Price> price) {
        while (!notifications->mutex.try_lock());
        for(auto it = notifications->multimap.begin(); it != notifications->multimap.end(); ++it){
            if(it->second.id == price->notify_id){
                it->second.prices = *price;
                break;
            }
        }
        notifications->mutex.unlock();
    }

    void Notifier::setPredictToNotify(std::shared_ptr<forms::Prediction> prediction) {
        while (!notifications->mutex.try_lock());
        for(auto it = notifications->multimap.begin(); it != notifications->multimap.end(); ++it){
            if(it->second.id == prediction->notify_id){
                it->second.prediction = *prediction;
                break;
            }
        }
        notifications->mutex.unlock();
    }

    void Notifier::deleteRequestNotify(unsigned int notify_id){
        while (!notifications->mutex.try_lock());
        for(auto it = notifications->multimap.begin(); it != notifications->multimap.end(); ++it){
            if(it->second.id == notify_id){
                notifications->multimap.erase(it);
                break;
            }
        }
        notifications->mutex.unlock();

        while (!crawlerReq->mutex.try_lock());
        for(auto it = crawlerReq->multimap.begin(); it != crawlerReq->multimap.end(); ++it){
            if(it->second.notify_id == notify_id){
                crawlerReq->multimap.erase(it);
                break;
            }
        }
        crawlerReq->mutex.unlock();
    }

    Notifier::~Notifier() {
        delete crawlerReq.get();
        delete notifications.get();
    }

    template<class T>
    void Notifier::incrementTime(std::_Rb_tree_iterator<std::pair<const unsigned long, T>> pair) {
        if (pair->second.periodic == forms::settings::everyDay)
            pair->second.closestTime += dayAddSec;
        if (pair->second.periodic == forms::settings::everyWeek)
            pair->second.closestTime += weekAddSec;
        if (pair->second.periodic == forms::settings::everyMonth)
            pair->second.closestTime += monthAddSec;
    }
}