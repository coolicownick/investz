//
// Created by nickolas on 04.12.2021.
//

#ifndef API_NOTIFIER_H
#define API_NOTIFIER_H
#pragma once
#include "forms.h"
#include "sync.http.client.h"
#include <map>
#include <mutex>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/asio.hpp>
#include <vector>
#include <condition_variable>

namespace notifications{
    const unsigned int limitCrawlerRecSec = 30; //180
    const unsigned int limitNotifySec = 10; //180
    const unsigned int nextNotyAddSec = 30; //180
    const unsigned int dayAddSec = 120; //3600 * 24;
    const unsigned int weekAddSec = 3600 * 24 * 7;
    const unsigned int monthAddSec = 3600 * 24 * 30;
    const auto crawlerPath = "/crawler/write";


    class Notifier: public client::syncHttpClient {
    public:
        Notifier();
        Notifier& operator=(const Notifier& n) = default;
        void run();
        void checkMaps();
        //add structs
        void addRequest(const forms::CrawlerRequest &request);
        void addNotification(const forms::Notification &notification);
        //add structs to notifier
        void setNewsToNotify(std::shared_ptr<std::vector<forms::News>> news);
        void setConsensusToNotify(std::shared_ptr<std::vector<forms::Consensus>> consensus);
        void setPriceToNotify(std::shared_ptr<forms::Price> price);
        void setPredictToNotify(std::shared_ptr<forms::Prediction> prediction);
        //delete notification
        void deleteRequestNotify(unsigned int notify_id);
        ~Notifier();

    private:

        template<class T>
        struct multimapPotected{
            std::multimap<unsigned long , T> multimap;
            std::mutex mutex;
            multimapPotected() = default;
        };
        std::unique_ptr<multimapPotected<forms::Notification>> notifications;
        std::unique_ptr<multimapPotected<forms::CrawlerRequest>> crawlerReq;
        std::shared_ptr<boost::asio::deadline_timer> timer;
        boost::asio::io_service io;
        std::condition_variable mapsCheck;
        void runTimerThread();
        std::condition_variable checkedBell;
        void sendCrawlerRequests();
        void sendNotifications();

        void clearNotifyData(forms::Notification &noty);
        bool setNextNotyIfStillData(forms::Notification &noty);

        template<class T>
        void incrementTime(std::_Rb_tree_iterator<std::pair<const unsigned long, T>> pair);
    };
}



#endif //API_NOTIFIER_H
