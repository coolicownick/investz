#ifndef TOOLS_H
#define TOOLS_H
#include <iostream>
template <typename T>
class Singleton {
 public:
//    static void Register(std::string name, T*);
    static T* Instance() {
        if (m_instance == nullptr) {
            m_instance = new T();
        }
        return m_instance;
    }
    static void Delete() {
        if (m_instance != nullptr) {
            std::cout << "m_instance=" << m_instance << std::endl;
            m_instance->~T();
            m_instance = nullptr;
            std::cout << "m_instance=" << m_instance << std::endl;
        }
    }
protected:
//    static Singleton* Lookup(std::string name);
    Singleton<T>() { };
private:
  static T* m_instance;
};

template <typename T>
T* Singleton<T>::m_instance = nullptr;

#endif // TOOLS_H
