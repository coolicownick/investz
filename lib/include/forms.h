#pragma once
#include <boost/property_tree/ptree.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace forms {
    namespace errors {
        constexpr auto errorStructField = "errorStruct";
        constexpr auto invalidTicker = "invalid ticker";
        constexpr auto invalidRegion = "invalid region";
        constexpr auto notFound = "not found";
    }// namespace errors
    namespace constants {
        constexpr auto OkMsg = "OK";
        constexpr auto targetPriceField = "targetPrice";
        constexpr auto summaryDetailField = "summaryDetail";
        constexpr auto resultField = "finance.result";
        constexpr auto recommendationField = "recommendation";
        constexpr auto ratingField = "rating";
        constexpr auto info_RecommendationField = "instrumentInfo.recommendation";
        constexpr auto reportsField = "reports";
        constexpr auto statusField = "status";
        constexpr auto messageField = "message";
        constexpr auto consensusField = "consensus";
        constexpr auto priceField = "price";
        constexpr auto newsField = "news";
        constexpr auto predictionField = "prediction";
        constexpr auto idField = "id";
        constexpr auto tickerField = "ticker";
        constexpr auto regionField = "region";
        constexpr auto titleField = "title";
        constexpr auto bodyField = "body";
        constexpr auto sourceField = "source";
        constexpr auto linkField = "link";
        constexpr auto uuidField = "uuid";
        constexpr auto authorField = "author";
        constexpr auto imageField = "image";
        constexpr auto tg_user_idField = "tg_user_id";
        constexpr auto usernameField = "username";
        constexpr auto firstnameField = "firstname";
        constexpr auto lastnameField = "lastname";
        constexpr auto closestTimeField = "closestTime";
        constexpr auto periodField = "period";
        constexpr auto periodicField = "periodic";
        constexpr auto settingsField = "settings";
        constexpr auto period_periodicField = "period.periodic";
        constexpr auto period_closestTimeField = "period.closestTime";
        constexpr auto summaryField = "summary";
        constexpr auto summary_periodicField = "summary.periodic";
        constexpr auto summary_closestTimeField = "summary.closestTime";
        constexpr auto typesField = "types";
        constexpr auto types_newsField = "types.news";
        constexpr auto types_consensusField = "types.consensus";
        constexpr auto types_priceField = "types.price";
        constexpr auto types_predictionField = "types.prediction";
        constexpr auto notify_idField = "notify_id";
        constexpr auto publisherField = "publisher";
        constexpr auto providerField = "provider";
        constexpr auto providerPublishTimeField = "providerPublishTime";
        constexpr auto publishedDateField = "publishedDate";
        constexpr auto createDateField = "createdDate";
        constexpr auto targetField = "target";
        constexpr auto settings_idField = "setting_id";
        constexpr auto actor_idField = "actor_id";
        constexpr auto periodTypeField = "periodType";
        constexpr auto crawlerRequestField = "crawlerRequest";
        constexpr auto dataField = "data";
        constexpr auto dateField = "date";
        constexpr auto contentsField = "contents";
        constexpr auto contentField = "content";
        constexpr auto defaultStringField = "";
        constexpr auto pathToImage = "body.data.partnerData.cover.image.originalUrl";
        constexpr auto maxNews = 4;
        constexpr auto maxConsensus = 4;
        constexpr auto defaultTypeIsJson = "application/json";
        constexpr auto nginx = "web";
        constexpr auto nginxPort = "80";
        constexpr auto api_setnews_target = "/api/setnews";
        constexpr auto api_setconsensus_target = "/api/setconsensus";
        constexpr auto api_setprediction_target = "/api/setprediction";
        constexpr auto api_setprice_target = "/api/setprice";
        constexpr auto crawler_consensus_target = "/crawler/consensus/last";
        constexpr auto crawler_newslast_target = "/crawler/news/last";
        constexpr auto crawler_prepareDataForMl_target = "/crawler/prepare/data";
        constexpr auto crawler_getprice_target = "/crawler/price";
        constexpr auto ml_pricePridection_target = "/ML/price/prediction";
        constexpr auto defaulRegionIsUS = "US";
        constexpr auto openField = "open";
        constexpr auto openRawField = "open.raw";
        constexpr auto tenDaysAverageVolumeRawField = "averageDailyVolume10Day.raw";
        constexpr auto lowField = "low";
        constexpr auto closeField = "close";
        constexpr auto volumeField = "volume";
        constexpr auto dividendsField = "dividends";
        constexpr auto previousCloseRawField = "previousClose.raw";
        constexpr auto priceRegularMarketOpenRawField = "price.regularMarketOpen.raw";
        constexpr auto regularMarketDayLowRawField = "price.regularMarketDayLow.raw";
        constexpr auto regularMarketPriceRawField = "regularMarketPrice.raw";
        constexpr auto regularMarketTimeField = "regularMarketTime";
        constexpr auto trailingAnnualDividendRateRawField = "trailingAnnualDividendRate.raw";
    }// namespace constants

    namespace endPointStructs {
        constexpr auto inputField = "input";
        struct InputData {
            InputData() = default;
            explicit InputData(const boost::property_tree::ptree& json);
            boost::property_tree::ptree getJson() const;
            std::string ticker;
            std::string region;
            unsigned int notify_id{0};
        };
    }// namespace endPointStructs

    namespace settings {
        struct Types {
            Types() = default;
            Types(const boost::property_tree::ptree& json);
            boost::property_tree::ptree getJson() const;
            bool consensus = false;
            bool price = false;
            bool news = false;
            bool prediction = false;
        };
        struct Setting {
            unsigned int id = 0;
            unsigned int user_tg_id = 0;
            std::string ticker;
            struct Period {
                Period() = default;
                Period(const boost::property_tree::ptree& json);
                boost::property_tree::ptree getJson() const;
                long closestTime = 0;
                unsigned int periodic = 0;
            } period;
            Types types;
            Period summary;
            Setting() = default;
            Setting(const boost::property_tree::ptree& pt);
            boost::property_tree::ptree getJson() const;
        };
        struct DeleteSetting {
            unsigned int user_tg_id = 0;
            std::string ticker;
            DeleteSetting() = default;
            DeleteSetting(const boost::property_tree::ptree& json);
            boost::property_tree::ptree getJson() const;
        };
        enum SubscribeTypes { news,
                              consensus,
                              price,
                              risk };
        enum Periodic { everyDay,
                        everyWeek,
                        everyMonth };
    }// namespace settings


    struct User {
        unsigned int id;
        unsigned int user_tg_id = 0;
        std::string username;
        std::string firstName;
        std::string lastName;
        User() = default;
        User(const boost::property_tree::ptree& pt);
        boost::property_tree::ptree getJson() const;
    };

    //constexpr auto newsStructField = "News";
    struct News {
        News() = default;
        News(const boost::property_tree::ptree& json);
        boost::property_tree::ptree getJson() const;
        unsigned int id{0};
        unsigned int notify_id{0};
        std::string title;
        std::string ticker;
        std::string body;
        std::string image;
        unsigned long publishedDate{0};
        std::string source;
        std::string author;
    };
    constexpr auto priceStructField = "Prices";
    struct Price {
        Price() = default;
        Price(const boost::property_tree::ptree& json);
        boost::property_tree::ptree getJson() const;
        unsigned int id{0};
        unsigned int notify_id{0};
        std::string ticker;
        float price{0};
        long date{0};
    };
    constexpr auto consensusStructField = "Consensuses";
    struct Consensus {
        Consensus() = default;
        Consensus(const boost::property_tree::ptree& json);
        boost::property_tree::ptree getJson() const;
        unsigned int id{0};
        unsigned int notify_id{0};
        std::string title;
        std::string ticker;
//        std::string image;
        std::string recommendation;
        long publishedDate{0};
//        std::string source;
        std::string body;
        std::string author;
        float target{0};
    };
    constexpr auto predictionStructField = "Prediction";
    struct Prediction {
        Prediction() = default;
        Prediction(const boost::property_tree::ptree& json);
        boost::property_tree::ptree getJson() const;
        unsigned int id{0};
        unsigned int notify_id{0};
        std::string ticker;
        float target{0};
    };

    struct Notification {
        Notification() = default;
        Notification(const boost::property_tree::ptree& json);
        boost::property_tree::ptree getJson() const;
        unsigned int id{0};
        std::string ticker;
        unsigned int tg_user_id;
        unsigned int actor_id;
        unsigned int periodic;
        unsigned long closestTime;
        std::vector<News> news;
        std::vector<Consensus> consensuses;
        Price prices;
        Prediction prediction;
        float target{0};
    };

    struct CrawlerRequest {
        CrawlerRequest() = default;
        CrawlerRequest(const boost::property_tree::ptree& json);
        boost::property_tree::ptree getJson() const;
        std::string ticker;
        settings::Types types;
        unsigned int notify_id{0};
        unsigned int periodic{0};
        unsigned long closestTime{0};
    };

    struct DataForML {
        DataForML() = default;
        DataForML(const boost::property_tree::ptree& json);
        boost::property_tree::ptree getJson() const;
        std::string ticker;
        float open{0};
        float price{0};
        float low{0};
        float close{0};
        float volume{0};
        float dividends{0};
    };

}// namespace forms
