#pragma once

#include <boost/property_tree/json_parser.hpp>
#include <iostream>
#include <set>
#include <string>

namespace tools {

    boost::property_tree::ptree toJson(const std::string& data);
    std::string toString(const boost::property_tree::ptree& data);

    template<typename T = std::string>
    void printJSON(boost::property_tree::ptree const& pt,
                   std::ostream& out,
                   std::set<T>* notPrintedFields = nullptr) {
        using boost::property_tree::ptree;
        ptree::const_iterator end = pt.end();
        for (ptree::const_iterator it = pt.begin(); it != end; ++it) {
            if (notPrintedFields == nullptr ||
                notPrintedFields->find(it->first) == notPrintedFields->end()) {
                out << it->first << ": " << it->second.get_value<std::string>()
                    << std::endl;
            }
            printJSON(it->second, out, notPrintedFields);
        }
    }

    constexpr auto publishedTime_field = "providerPublishTime: ";
    constexpr auto digits = "0123456789";
    void convertEpochToUtc(std::string& report);

}// namespace tools
