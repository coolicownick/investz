#ifndef STOCKSCOMPONENTS_H
#define STOCKSCOMPONENTS_H
#include <string>
#include <vector>
enum subsribeTypes {consensuns, price, news, risk};
struct Settings {
    std::string m_ticker;
    struct Period {
        long m_closestTime;
        bool m_everyDay;
        bool m_everyWeek;
        bool m_everyMonth;
    } m_period;
    std::vector<subsribeTypes> types; //consensuns, price, news, risk
    Period summary;
};

struct News {
    std::string m_title;
    std::string m_ticker;
    std::string m_body;
    std::string m_image;
    long m_publishedDate;
    std::string m_source;
    std::string m_author;
};

struct Consensuns {
    std::string m_title;
    std::string m_ticker;
    std::string m_image;
    long m_publishedDate;
    std::string m_source;
    std::string m_author;
    std::string m_target;
};
#endif // STOCKCOMPONENTS_H
