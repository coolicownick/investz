#ifndef HEAP_H
#define HEAP_H
#include <iomanip>
#include "tools.h"
#define _heap_size_multuply_koeff 2
template <class T, class Comparator /* = DefaultProcessComparator__lt*/>
class Heap {
 public:
  const T& top() const;
  void pop();
  void push(const T&& element);
  size_t getSize() const;
  bool empty() const;
  bool full() const;

 private:
  Heap();
  Heap(T* _buffer, size_t _size);
  ~Heap();
  explicit Heap(Heap<T, Comparator>&& _heap);
  friend Singleton<Heap>;
  T* m_buffer;
  size_t m_size;
  size_t m_capacity;
  Heap(const Heap&) = delete;
  Heap& operator=(const Heap&) = delete;
  Heap& operator=(Heap&&) = delete;
  Comparator cmp;
  void siftUp(size_t idx);
  void siftDown(size_t idx);
  void buildHeap();
  void grow();
};

#endif // HEAP_H
