#include <limits>
#include "heap.h"

template <class T, class Comparator /* = DefaultProcessComparator__lt*/>
  Heap<T, Comparator>::Heap(T* _buffer, size_t _size)
      : m_buffer(_buffer), m_size(_size), m_capacity(0) {
    buildHeap();
  };

  template <class T, class Comparator /* = DefaultProcessComparator__lt*/>
  Heap<T, Comparator>::Heap(Heap<T, Comparator>&& _heap) {
    this->m_buffer = _heap.m_buffer;
    this->m_size = _heap.m_size;
    this->m_capacity = _heap.m_capacity;
    _heap.m_buffer = nullptr;
  }

  template <class T, class Comparator /* = DefaultProcessComparator__lt*/>
  Heap<T, Comparator>::~Heap() { delete[] m_buffer; }

  template <class T, class Comparator /* = DefaultProcessComparator__lt*/>
  const T& Heap<T, Comparator>::top() const { return m_buffer[0]; }

  template <class T, class Comparator /* = DefaultProcessComparator__lt*/>
  void Heap<T, Comparator>::pop() {
    assert(!empty());
    auto result = m_buffer[0];
    m_buffer[0] = std::move(m_buffer[--m_size]);
    m_capacity++;
    if (!empty()) {
      siftDown(0);
    }
  };

  template <class T, class Comparator /* = DefaultProcessComparator__lt*/>
  void Heap<T, Comparator>::push(const T&& element) {
    if (full()) {
      grow();
      return push(std::move(element));
    }
    m_buffer[m_size++] = element;
    m_capacity--;
    siftUp(m_size - 1);
  }

  template <class T, class Comparator /* = DefaultProcessComparator__lt*/>
  size_t Heap<T, Comparator>::getSize() const { return m_size; }

  template <class T, class Comparator /* = DefaultProcessComparator__lt*/>
  bool Heap<T, Comparator>::empty() const { return m_size == 0; }

  template <class T, class Comparator /* = DefaultProcessComparator__lt*/>
  bool Heap<T, Comparator>::full() const { return m_capacity == 0; }

  template <class T, class Comparator /* = DefaultProcessComparator__lt*/>
  void Heap<T, Comparator>::siftUp(size_t idx) {
    if (idx != 0) {
      size_t parent{(idx - 1) / 2};
      if (parent > 0) {
        if (cmp(m_buffer[parent], m_buffer[idx])) {
          return;
        }
        std::swap(m_buffer[idx], m_buffer[parent]);
        idx = parent;
      }
    }
  }

  template <class T, class Comparator /* = DefaultProcessComparator__lt*/>
  void Heap<T, Comparator>::siftDown(size_t idx) {
    size_t left{2 * idx + 1};
    size_t right{2 * idx + 2};
    size_t smallest{idx};
    if (left < m_size && cmp(m_buffer[left], m_buffer[idx])) {
      smallest = left;
    } else if (right < m_size && cmp(m_buffer[right], m_buffer[idx])) {
      smallest = right;
    }
    if (smallest != idx) {
      std::swap(m_buffer[idx], m_buffer[smallest]);
      siftDown(smallest);
    }
  }

  template <class T, class Comparator /* = DefaultProcessComparator__lt*/>
  void Heap<T, Comparator>::buildHeap() {
    // так как size_t при декрементировании нуля уходит в максимальное значение
    for (size_t i{m_size / 2 - 1}; i != std::numeric_limits<size_t>::max();
         --i) {
      siftDown(i);
    }
  }

  template <class T, class Comparator /* = DefaultProcessComparator__lt*/>
  void Heap<T, Comparator>::grow() {
    size_t newSize = m_size * _heap_size_multuply_koeff;
    auto newBuffer{new T[newSize]};
    for (size_t i{0}; i < m_size; i++) {
      newBuffer[i] = m_buffer[i];
    }
    delete[] m_buffer;
    m_buffer = newBuffer;
    m_capacity += newSize - m_size;
    m_size = newSize;
    newBuffer = nullptr;
  }
