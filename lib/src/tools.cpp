#include "tools.h"

namespace tools {

    boost::property_tree::ptree toJson(const std::string& data) {
        std::stringstream ss(data);
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss, pt);
        return pt;
    }

    std::string toString(const boost::property_tree::ptree& data) {
        std::stringstream ss;
        boost::property_tree::write_json(ss, data);
        return ss.str();
    }

    void convertEpochToUtc(std::string& report) {
        size_t startPos{report.find(publishedTime_field)};
        while (startPos != std::string::npos) {
            startPos = report.find_first_of(digits, startPos);
            size_t endPos = report.find_first_not_of(digits, startPos);
            size_t length = endPos - startPos + 1;
            auto epochTime {time_t(std::atoi(report.substr(startPos, length).c_str()))};
            auto UTC {asctime(gmtime(&epochTime))};
            report.replace(startPos, length, UTC);
            startPos = report.find(publishedTime_field, startPos);
        }
    }
}


