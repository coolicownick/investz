#include "forms.h"

#include <boost/property_tree/json_parser.hpp>
#include <boost/optional/optional.hpp>

using boost::property_tree::ptree;
using forms::endPointStructs::InputData;

using namespace forms::constants;

namespace forms::endPointStructs {
    InputData::InputData(const ptree &json) {
        auto child = json.get_child(inputField);
        ticker = child.get<std::string>(tickerField, defaultStringField);
        region = child.get<std::string>(regionField, defaultStringField);
        notify_id = child.get<unsigned int>(notify_idField, 0);
        std::cout << notify_id << std::endl;

    }
    ptree InputData::getJson() const {
        boost::property_tree::ptree child;
        child.put(tickerField, ticker);
        child.put(regionField, region);
        child.put(notify_idField, notify_id);
        ptree json;
        json.put_child(inputField, child);
        return json;
    }
}// namespace forms::endPointStructs

namespace forms {

    User::User(const ptree &pt) {
        id = pt.get<unsigned int>(tg_user_idField, 0);
        username = pt.get<std::string>(usernameField, "");
        firstName = pt.get<std::string>(firstnameField, "");
        lastName = pt.get<std::string>(lastnameField, "");
    }

    ptree User::getJson() const {
        ptree pt;
        pt.add(tg_user_idField, id);
        pt.add(usernameField, username);
        pt.add(firstnameField, firstName);
        pt.add(lastnameField, lastName);
        return pt;
    }

    namespace settings {
        Types::Types(const ptree &json) {
            consensus = json.get<bool>(consensusField, false);
            price = json.get<bool>(priceField, false);
            news = json.get<bool>(newsField, false);
            prediction = json.get<bool>(predictionField, false);
        }
        ptree Types::getJson() const {
            ptree json;
            json.put(consensusField, consensus);
            json.put(priceField, price);
            json.put(newsField, news);
            json.put(predictionField, prediction);
            return json;
        }

        Setting::Period::Period(const boost::property_tree::ptree &json) {
            closestTime = json.get<long>(closestTimeField);
            periodic = json.get<unsigned int>(periodicField);
        }

        ptree Setting::Period::getJson() const {
            ptree json;
            json.put(closestTimeField, closestTime);
            json.put(periodicField, periodic);
            return json;
        }


        Setting::Setting(const ptree &pt) {
            id = pt.get<unsigned int>(idField, 0);
            user_tg_id = pt.get<unsigned int>(tg_user_idField, 0);
            ticker = pt.get<std::string>(tickerField, "");
            period.periodic = pt.get<unsigned int>(period_periodicField, 0);
            period.closestTime = pt.get<long>(period_closestTimeField, 0);
            summary.periodic = pt.get<unsigned int>(summary_periodicField, 0);
            summary.closestTime = pt.get<long>(summary_closestTimeField, 0);
            types.news = pt.get<bool>(types_newsField, 0);
            types.consensus = pt.get<bool>(types_consensusField, 0);
            types.price = pt.get<bool>(types_priceField, 0);
            types.prediction = pt.get<bool>(types_predictionField, 0);
        }

        ptree Setting::getJson() const {

            ptree pt;
            pt.add(idField, id);
            pt.add(tg_user_idField, user_tg_id);
            pt.add(tickerField, ticker);
            ptree tempPeriod;
            tempPeriod.add(periodField,period.periodic);
            tempPeriod.add(closestTimeField, period.closestTime);
            pt.add_child(periodField, tempPeriod);

            ptree period_summary;
            period_summary.add(periodicField, summary.periodic);
            period_summary.add(closestTimeField, summary.closestTime);
            pt.add_child(summaryField, period_summary);

            ptree tempTypes = types.getJson();
            pt.add_child(typesField, tempTypes);
            return pt;
        }
        DeleteSetting::DeleteSetting(const ptree &pt){
            user_tg_id = pt.get<unsigned int>(tg_user_idField,0);
            ticker = pt.get<std::string>(tickerField, "");
        }

        boost::property_tree::ptree DeleteSetting::getJson() const {
            ptree pt;
            pt.add(tg_user_idField,user_tg_id);
            pt.add(tickerField,ticker);
            return pt;
        }
    }// namespace settings

    News::News(const ptree &json) {
        id = json.get<unsigned int>(idField, 0);
        notify_id = json.get<unsigned int>(notify_idField, 0);
        title = json.get<std::string>(titleField, "");
        ticker = json.get<std::string>(tickerField, "");
        body = json.get<std::string>(bodyField, "");
        image = json.get<std::string>(imageField, "");
        publishedDate = json.get<unsigned long>(publishedDateField, 0);
        source = json.get<std::string>(sourceField, "");
        author = json.get<std::string>(authorField, "");
    }

    ptree News::getJson() const {
        ptree json;
        json.put(idField, id);
        json.put(notify_idField, notify_id);
        json.put(titleField, title);
        json.put(tickerField, ticker);
        json.put(bodyField, body);
        json.put(imageField, image);
        json.put(publishedDateField, publishedDate);
        json.put(sourceField, source);
        json.put(authorField, author);
        return json;
    }
    Consensus::Consensus(const boost::property_tree::ptree &json) {
        id = json.get<unsigned int>(idField, 0);
        notify_id = json.get<unsigned int>(notify_idField, 0);
        title = json.get<std::string>(titleField, "");
        ticker = json.get<std::string>(tickerField, "");
//        image = json.get<std::string>(imageField, "");
        target = json.get<float>(targetField,0);
        recommendation = json.get<std::string>(recommendationField, "");
        publishedDate = json.get<unsigned long>(publishedDateField, 0);
//        source = json.get<std::string>(sourceField, "");
        author = json.get<std::string>(authorField, "");
        body = json.get<std::string>(bodyField, "");
    }

    boost::property_tree::ptree Consensus::getJson() const {
        ptree json;
        json.put(idField, id);
        json.put(notify_idField, notify_id);
        json.put(titleField, title);
        json.put(tickerField, ticker);
//        json.put(imageField, image);
        json.put(targetField, target);
        json.put(recommendationField, recommendation);
        json.put(publishedDateField, publishedDate);
//        json.put(sourceField, source);
        json.put(authorField, author);
        json.put(bodyField, body);
        return json;
    }
    Price::Price(const boost::property_tree::ptree &json) {
        id = json.get<unsigned int>(idField, 0);
        notify_id = json.get<unsigned int>(notify_idField, 0);
        ticker = json.get<std::string>(tickerField, "");
        price = json.get<float>(priceField,0);
        date= json.get<unsigned long>(dateField, 0);

    }

    boost::property_tree::ptree Price::getJson() const {
        ptree json;
        json.put(idField, id);
        json.put(notify_idField, notify_id);
        json.put(tickerField, ticker);
        json.put(dateField, date);
        json.put(priceField, price);
        return json;
    }

    Prediction::Prediction(const boost::property_tree::ptree &json) {
        id = json.get<unsigned int>(idField, 0);
        notify_id = json.get<unsigned int>(notify_idField, 0);
        ticker = json.get<std::string>(tickerField, "");
        target = json.get<float>(targetField,0);
    }

    boost::property_tree::ptree Prediction::getJson() const {
        ptree json;
        json.put(idField, id);
        json.put(notify_idField, notify_id);
        json.put(tickerField, ticker);
        json.put(targetField, target);
        return json;
    }
    Notification::Notification(const ptree &json) {
        boost::optional< const ptree& > child;
        id = json.get<unsigned int>(idField, 0);
        ticker = json.get<std::string>(tickerField, defaultStringField);
        tg_user_id = json.get<unsigned int>(tg_user_idField, 0);
        actor_id = json.get<unsigned int>(actor_idField, 0);
        periodic = json.get<unsigned int>(periodTypeField, 0);
        child = json.get_child_optional( newsField);
        if(child){
            auto newsVector = std::vector<forms::News>();
            for (auto &pair: json.get_child(newsField)) {
                auto news = News(pair.second);
                newsVector.push_back(news);
            }
            news = newsVector;
        }
        child = json.get_child_optional(consensusField);
        if(child){
            auto consensusVector = std::vector<forms::Consensus>();
            for (auto &pair: json.get_child(consensusField)) {
                auto consensus = Consensus(pair.second);
                consensusVector.push_back(consensus);
            }
            consensuses = consensusVector;
        }
        prices = Price(json.get_child(priceField));
        prediction = Prediction(json.get_child(predictionField));
    }

    ptree Notification::getJson() const {
        ptree json;
        json.put(idField, id);
        json.put(tickerField, ticker);
        json.put(tg_user_idField, tg_user_id);
        json.put(actor_idField, actor_id);
        json.put(periodTypeField, periodic);
        ptree json_news;
        for (auto &n: news) {
            json_news.push_back(std::make_pair("",n.getJson()));
        }
        json.add_child(newsField,json_news);
        ptree json_consensus;
        for (auto &cons: consensuses) {
            json_consensus.push_back(std::make_pair("",cons.getJson()));
        }
        json.add_child(consensusField,json_consensus);

        json.add_child(priceField,prices.getJson());
        json.add_child(predictionField,prediction.getJson());
        return json;
    }

    CrawlerRequest::CrawlerRequest(const ptree &json) {
        ticker = json.get<std::string>(tickerField);

        types.news = json.get<bool>(types_newsField, false);
        types.consensus = json.get<bool>(types_consensusField, false);
        types.price = json.get<bool>(types_priceField, false);
        types.prediction = json.get<bool>(types_predictionField, false);

        notify_id = json.get<unsigned int>(notify_idField, 0);
        periodic = json.get<unsigned int>(periodicField, 0);
        closestTime = json.get<unsigned int>(closestTimeField, 0);
    }

    ptree CrawlerRequest::getJson() const {
        ptree json;
        json.put(tickerField, ticker);

        ptree tempTypes = types.getJson();

        json.add_child(typesField, tempTypes);


        json.put(notify_idField, notify_id);
        json.put(periodicField, periodic);
        json.put(closestTimeField, closestTime);

        return json;
    }

    DataForML::DataForML(const boost::property_tree::ptree &json) {
        ticker = json.get<std::string>(tickerField, "");
        open = json.get<float>(openField, 0);
        price = json.get<float>(priceField, 0);
        low = json.get<float>(lowField, 0);
        close = json.get<float>(closeField, 0);
        volume = json.get<float>(volumeField, 0);
        dividends = json.get<float>(dividendsField, 0);
    }

    boost::property_tree::ptree DataForML::getJson() const {
        ptree json;
        json.put(tickerField, ticker);
        json.put(openField, open);
        json.put(priceField, price);
        json.put(lowField, low);
        json.put(closeField, close);
        json.put(volumeField, volume);
        json.put(dividendsField, dividends);
        return json;
    }
}// namespace forms

