//
// Created by nickolas on 26.11.2021.
//
#include "serverBot.h"
#include "sync.http.client.h"
#include "forms.h"

using namespace TgBot;
namespace serverBot {

    void InvestzBot::handleAnyMessage(
            const Message::Ptr &message, const ReplyKeyboardRemove::Ptr &removeKeyboard,
            const InlineKeyboardMarkup::Ptr &contentTypeKeyboard) {
        if (StringTools::startsWith(message->text, "Signup")) {
            handleUserSignup(message, removeKeyboard);
            return;
        }

        if (StringTools::startsWith(message->text, "Login")) {
            handleUserLogin(message);
            return;
        }
        handleTickerSetup(message, contentTypeKeyboard);
    }

    void InvestzBot::handleUserSignup(
            const Message::Ptr &message,
            const ReplyKeyboardRemove::Ptr &removeKeyboard) {
        auto tgUser(message->from);
        ::User user;
        user.id = tgUser->id;
        user.firstName = tgUser->firstName;
        user.lastName = tgUser->lastName;
        user.username = tgUser->username;

        tgBot.getApi().sendMessage(tgUser->id, botSettings["register_succeed"], false,
                                   0, removeKeyboard);

        {
            // TODO send user to server
        }

        std::vector<Message::Ptr> history;
        userHistory->insert({tgUser->id, history});
    }

    void InvestzBot::handleUserLogin(const Message::Ptr &message) {
        // TODO Create user check
    }

    void InvestzBot::handleTickerSetup(
            const Message::Ptr &message,
            const InlineKeyboardMarkup::Ptr &contentTypeKeyboard) {
        // Handle already registered
        auto userID = message->from->id;
        Message::Ptr prevMsg = nullptr;

        auto &history = userHistory->at(userID);

        if (!history.empty()) prevMsg = history.back();
        std::cout << "prev: " << prevMsg << std::endl;
        if (prevMsg) {
            auto setting = forms::settings::Setting();
            setting.ticker = message->text;
            {
                // TODO check for ticker;
            }
            tgBot.getApi().sendMessage(userID, botSettings["choose_info"], false, 0,
                                       contentTypeKeyboard);
            if (userOpen->find(userID) == userOpen->end())
                userOpen->insert({userID, true});
            else
                userOpen->at(userID) = true;
            if (userSettings->find(userID) == userSettings->end()) {
                std::vector<forms::settings::Setting> history;
                userSettings->insert({userID, history});
            }
            userSettings->at(userID).push_back(setting);
        }
    }

    void InvestzBot::handleDemo(const Message::Ptr &message,
                                const ReplyKeyboardRemove::Ptr &removeKeyboard) {
        auto msg = StringTools::split(message->text, ' ');

        if (msg[0] != "get") {
            tgBot.getApi().sendMessage(message->chat->id, botSettings["wrong_format"],
                                       false, message->messageId, removeKeyboard,
                                       "Markdown");
            return;
        }
        auto ticker = msg[1];
        auto region = "US";
        boost::property_tree::ptree pt;
        pt.put("ticker",ticker);
        pt.put("region",region);
        std::stringstream json;
        boost::property_tree::write_json(json, pt, false);
        boost::asio::io_context io_context;
        client::syncHttpClient httpClient(io_context);
        //TODO add to client
        auto body = json.str();
        *body.rbegin() = '\0';

        std::cout << body << std::endl;
        httpClient.sendRequest("web","80","/crawler/news/last", body.c_str());

        auto reply = "Нашлась следующая информация по тикеру `" + msg[1] +
                     "` за последний день " + "\nНовости:\n";
        reply += "\nПрогнозы:\n";
        reply += "\nЦена:\n";
        reply += "\nРиски:\n";
        tgBot.getApi().sendMessage(message->chat->id, reply, false,
                                   message->messageId, removeKeyboard, "Markdown");
    }

}  // namespace serverBot
