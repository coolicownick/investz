#!/bin/bash

rm -rf build && mkdir build && cd build
cmake ..
make -j4
cp ../settings.toml ./
./botTG &
pid="$!"

while true
do
  if [ -d /proc/$pid ]; then
    wait "$pid"
    sleep 1
  else
    make -j4
    ./botTG &
    pid="$!"
  fi
done
