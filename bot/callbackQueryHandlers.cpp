//
// Created by nickolas on 27.11.2021.
//

#include "serverBot.h"

using forms::settings::SubscribeTypes;

using namespace TgBot;
namespace serverBot {
    void InvestzBot::handleCallbackQuery(
            const TgBot::CallbackQuery::Ptr &query,
            const TgBot::InlineKeyboardMarkup::Ptr &periodKeyboard) {
        auto userID = query->message->chat->id;
        if (!userOpen->at(userID)) return;
        std::cout << "data: " << query->data << std::endl;

        // query/1
        auto &setting = userSettings->at(userID).back();
        auto callbacks = StringTools::split(query->data, '/');
        auto num = std::stoi(callbacks[1]);
        int typesSize = sizeof(setting.types) / sizeof(bool);

        if (callbacks[0] == "period") {
            setting.period.periodic = num;
            tgBot.getApi().sendMessage(query->message->chat->id, "Интервал задан");
            std::cout << "settings: "
                      << "news: " << setting.types.news
                      << " consensus: " << setting.types.consensus
                      << " price: " << setting.types.price << " risk: " << setting.types.prediction
                      << " ticker: " << setting.ticker
                      << " period: " << setting.period.periodic << std::endl;
            tgBot.getApi().sendMessage(query->message->chat->id,
                                       "Настройка завершена!");
            userOpen->at(userID) = false;
        }
        if (num < typesSize && callbacks[0] == "content")
            switch (num) {
                case SubscribeTypes::news:{
                    setting.types.news = true;
                    break;
                }
                case SubscribeTypes::consensus:{
                    setting.types.consensus = true;
                    break;
                }
                case SubscribeTypes::price:{
                    setting.types.price = true;
                    break;
                }
                case SubscribeTypes::risk:{
                    setting.types.prediction = true;
                    break;
                }

            }
        std::cout << "settings: "
                  << "news: " << setting.types.news
                  << " consensus: " << setting.types.consensus
                  << " price: " << setting.types.price << " risk: " << setting.types.prediction
                  << std::endl;
        if (StringTools::endsWith(query->data, std::to_string(typesSize))) {
            tgBot.getApi().sendMessage(query->message->chat->id, "Типы заданы!");

            tgBot.getApi().sendMessage(query->message->chat->id,
                                       "Задайте интервал: ", false, 0, periodKeyboard);
        }
    }

}  // namespace serverBot