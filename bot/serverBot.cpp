//
// Created by nickolas on 10.12.2021.
//

#include "serverBot.h"
#include "forms.h"

namespace serverBot {


    bot::InvestzBot Investz::investBot = bot::InvestzBot();

    Investz::Investz(const std::string &address, const std::string &port, std::size_t thread_pool_size) :
            server::asyncHttpServer(address, port, thread_pool_size ? get_nprocs() : thread_pool_size) {

        std::thread botThread([&](){
            Investz::investBot.run();
        });

        botThread.detach();
        request_router.addHandler("/notification", notificationHandler);
        std::cout << "handler notification started " << std::endl;
    }

    server::Response notificationHandler(const server::Request &request) {
        std::cout << "req" << std::endl;
        if (to_string(request.method()) != "POST")
            return server::Response{http::status::method_not_allowed, request.version()};
        server::Response response{http::status::bad_request, request.version()};
        std::stringstream ss(request.body().data());
        std::cout << request << std::endl;
        std::stringstream out;
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss,pt);
        auto notification = std::make_shared<forms::Notification>(pt);
        std::cout << "recieved " << notification->id << std::endl;
        Investz::investBot.sendNotify(notification);
        response.result(http::status::ok);
        return response;
    }
}