//
// Created by nickolas on 27.11.2021.
//
#include "serverBot.h"

using namespace TgBot;
namespace serverBot {

    void InvestzBot::createOneColumnKeyboard(
            const std::vector<std::string> &buttonStrings,
            TgBot::ReplyKeyboardMarkup::Ptr &kb) {
        for (size_t i = 0; i < buttonStrings.size(); ++i) {
            std::vector<TgBot::KeyboardButton::Ptr> row;
            TgBot::KeyboardButton::Ptr button(new TgBot::KeyboardButton);
            button->text = buttonStrings[i];
            row.push_back(button);
            kb->keyboard.push_back(row);
        }
    }

    void InvestzBot::createOneColumnInlineKeyboard(
            const std::vector<std::string> &buttonStrings,
            const std::string &callbackName, InlineKeyboardMarkup::Ptr &kb) {
        for (size_t i = 0; i < buttonStrings.size(); ++i) {
            std::vector<InlineKeyboardButton::Ptr> row;
            InlineKeyboardButton::Ptr button(new InlineKeyboardButton);
            button->text = buttonStrings[i];
            button->callbackData = callbackName + "/" + std::to_string(i);
            row.push_back(button);
            kb->inlineKeyboard.push_back(row);
        }
    }

    bool InvestzBot::checkUserExistsCache(const Message::Ptr &message) {
        if (userHistory->find(message->from->id) == userHistory->end() &&
            (!StringTools::startsWith(message->text, "Signup") &&
             !StringTools::startsWith(message->text, "Login"))) {
            tgBot.getApi().sendMessage(
                    message->chat->id,
                    "Вы не авторизованы. Пожалуйста введите команду /start");
            return false;
        }
        return true;
    }
}  // namespace serverBot
