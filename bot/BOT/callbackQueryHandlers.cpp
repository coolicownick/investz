//
// Created by nickolas on 27.11.2021.
//

#pragma once

#include "bot.h"

using forms::settings::SubscribeTypes;

using namespace TgBot;
namespace bot {
    void InvestzBot::handleCallbackQuery(
            const TgBot::CallbackQuery::Ptr &query,
            const TgBot::InlineKeyboardMarkup::Ptr &periodKeyboard) {
        auto userID = query->message->chat->id;
        auto callbacks = StringTools::split(query->data, '/');
        auto num = boost::lexical_cast<unsigned int>(callbacks[1]);
        if(callbacks[0] == "setting"){
            handleUpdateCallback(num,query);
        }
        while (!userOpen->mutex.try_lock());
        auto user =userOpen->map.at(userID);
        userOpen->mutex.unlock();
        if (!user) return;
        while (!userSettings->mutex.try_lock());
        // query/1
        auto &setting = userSettings->map.at(userID).back();
        unsigned int typesSize = sizeof(setting.types) / sizeof(bool);

        if (callbacks[0] == "period") {
            setting.period.periodic = num;
            tgBot.getApi().sendMessage(query->message->chat->id, "Интервал задан");
            tgBot.getApi().sendMessage(query->message->chat->id,
                                       "Настройка завершена!");
            while(!userOpen->mutex.try_lock());
            userOpen->map.at(userID) = false;
            userOpen->mutex.unlock();

            std::stringstream requestBody;
            boost::property_tree::write_json(requestBody, setting.getJson());
            std::cout << "request: " << requestBody.str().c_str() << std::endl;
            auto response = httpClient.sendRequest("web", http::verb::post, "80", "/api/setsubscribe",
                                                   requestBody.str().c_str());
            if (response.result() != http::status::ok){
                tgBot.getApi().sendMessage(query->message->chat->id,
                                           "Настройка не удалась!");
            }

        }
        if (num < typesSize && callbacks[0] == "content")
            switch (num) {
                case SubscribeTypes::news:{
                    setting.types.news = true;
                    break;
                }
                case SubscribeTypes::consensus:{
                    setting.types.consensus = true;
                    break;
                }
                case SubscribeTypes::price:{
                    setting.types.price = true;
                    break;
                }
                case SubscribeTypes::risk:{
                    setting.types.prediction = true;
                    break;
                }
            }

        userSettings->mutex.unlock();
        //send msg;


        if (StringTools::endsWith(query->data, std::to_string(typesSize))) {
            tgBot.getApi().sendMessage(query->message->chat->id, "Типы заданы!");

            tgBot.getApi().sendMessage(query->message->chat->id,
                                       "Задайте интервал: ", false, 0, periodKeyboard);
        }

    }
    void InvestzBot::handleUpdateCallback(unsigned int &commandNum,const TgBot::CallbackQuery::Ptr &query) {
        while (!userHistory->mutex.try_lock());
        auto messages = userHistory->map.at(query->from->id);
        userHistory->mutex.unlock();
        std::cout << "handle messages empty" << std::endl;

        if(messages.empty())
            return;
        try{
            auto settingNum = boost::lexical_cast<unsigned int>(messages.back()->text) - 1;
            std::cout << "handle callback update" << std::endl;
            while(!userSettings->mutex.try_lock());
            auto& settings = userSettings->map.at(query->from->id);
            if(settings.empty()){
                userSettings->mutex.unlock();
                return;
            }
            auto singleSetting = settings.at(settingNum);
            auto deleteSetting = forms::settings::DeleteSetting();
            deleteSetting.user_tg_id = query->from->id;
            deleteSetting.ticker = singleSetting.ticker;
            std::stringstream requestBody;
            boost::property_tree::write_json(requestBody, deleteSetting.getJson());
            auto response = httpClient.sendRequest("web", http::verb::post, "80", "/api/deletesubscribe",
                                                   requestBody.str().c_str());
            if (response.result() != http::status::ok){
                tgBot.getApi().sendMessage(query->from->id,
                                           "Не получается произвести обновление. Попробуйте позже");
                userSettings->mutex.unlock();
                return;
            }
            settings.erase(settings.begin() + settingNum);
            if(commandNum == 0){ // update
                tgBot.getApi().sendMessage(query->message->chat->id,
                                           "Задайте новые параметры настройки");
                handleCommandNew(query->message);
            }
            if(commandNum == 1){ //
                tgBot.getApi().sendMessage(query->message->chat->id,
                                           "Настройка успешно удалена");
            }
            userSettings->mutex.unlock();
        }catch (boost::bad_lexical_cast const &e){
            std::cout << e.what() << " printed: " << messages.back()->text << std::endl;
            userSettings->mutex.unlock();
        }
    }

}  // namespace serverBot