//
// Created by nickolas on 26.11.2021.
//

#pragma once

#include "bot.h"
#include "forms.h"

using namespace TgBot;
namespace bot {

    bool InvestzBot::checkUserServer(unsigned int id) {
        boost::property_tree::ptree pt;
        pt.put("tg_user_id", id);
        std::stringstream requestBody;
        boost::property_tree::write_json(requestBody, pt);
        auto response = httpClient.sendRequest("web", http::verb::post, "80", "/api/getuser",
                                               requestBody.str().c_str());
        if (response.result() != http::status::ok)
            return false;
        pt.erase(pt.begin());
        auto responseBody = std::stringstream(boost::beast::buffers_to_string(response.body().data()));
        boost::property_tree::read_json(responseBody, pt);

        auto user = forms::User(pt);

        if (user.id == id)
            return true;
        return false;
    }
    void InvestzBot::loadSettingsServer(unsigned int id){

        boost::property_tree::ptree pt;
        pt.put("user_tg_id", id);
        std::stringstream requestBody;
        boost::property_tree::write_json(requestBody, pt);
        auto response = httpClient.sendRequest("web", http::verb::post, "80", "/api/getsubscribe",
                                               requestBody.str().c_str());
        if (response.result() != http::status::ok)
            return;
        auto responseBody = std::stringstream(boost::beast::buffers_to_string(response.body().data()));
        boost::property_tree::ptree json;
        boost::property_tree::read_json(responseBody, json);
        boost::optional< const boost::property_tree::ptree& > child;
        child = json.get_child_optional(forms::constants::settingsField);
        auto settings = std::vector<forms::settings::Setting>();
        if(child) {
            for (auto &pair: json.get_child(forms::constants::settingsField)) {
                auto set = forms::settings::Setting(pair.second);
                std::cout << "found: " << set.id << std::endl;
                settings.push_back(set);
            }
        }

        while (!userSettings->mutex.try_lock());
        std::cout << "try to insert" << std::endl;
        userSettings->map.insert({id,settings});
        userSettings->mutex.unlock();

    }

    void InvestzBot::handleAnyMessage(
            const Message::Ptr &message, const ReplyKeyboardRemove::Ptr &removeKeyboard,
            const InlineKeyboardMarkup::Ptr &contentTypeKeyboard) {
        if (StringTools::startsWith(message->text, "Login")) {
            std::cout << "Login" << std::endl;
            handleUserLogin(message, removeKeyboard);
            return;
        }
        if (StringTools::startsWith(message->text, "Logout")) {
            std::cout << "Logout" << std::endl;
            handleUserLogout(message, removeKeyboard);
            return;
        }
        std::cout << "user exists" << std::endl;
        if (!checkUserExistsCache(message)) {

            tgBot.getApi().sendMessage(
                    message->chat->id,
                    botSettings["not_authorized"]);
            return;
        }
        std::cout << "Ticker setup" << std::endl;
        handleTickerSetup(message, contentTypeKeyboard);
    }

    void InvestzBot::handleUserLogin(const Message::Ptr &message, const ReplyKeyboardRemove::Ptr &removeKeyboard) {
        if (checkUserExistsCache(message)) {
            tgBot.getApi().sendMessage(message->chat->id, botSettings["register_declined"], false, 0, removeKeyboard);
            return;
        }
        auto tgUser(message->from);
        if (checkUserServer(tgUser->id)) {
            tgBot.getApi().sendMessage(tgUser->id, botSettings["register_succeed"], false, 0, removeKeyboard);
            std::vector<Message::Ptr> history;
            while (!userHistory->mutex.try_lock());
            userHistory->map.insert({tgUser->id, history});
            userHistory->mutex.unlock();
            loadSettingsServer(tgUser->id);
            return;
        }

        auto userForm = forms::User();
        userForm.id = tgUser->id;
        userForm.username = tgUser->username;
        userForm.firstName = tgUser->firstName;
        userForm.lastName = tgUser->lastName;
        std::stringstream requestBody;
        boost::property_tree::write_json(requestBody, userForm.getJson());
        auto response = httpClient.sendRequest("web", http::verb::post, "80", "/api/setuser",
                                               requestBody.str().c_str());
        if (response.result() == http::status::ok) {
            tgBot.getApi().sendMessage(tgUser->id, botSettings["register_succeed"], false,
                                       0, removeKeyboard);
        } else {
            tgBot.getApi().sendMessage(tgUser->id, botSettings["register_trouble"], false, 0, removeKeyboard);
        }

        std::vector<Message::Ptr> history;
        while (!userHistory->mutex.try_lock());
        userHistory->map.insert({tgUser->id, history});
        userHistory->mutex.unlock();
    }

    void InvestzBot::handleUserLogout(const TgBot::Message::Ptr &message,
                                      const TgBot::ReplyKeyboardRemove::Ptr &removeKeyboard) {
        while (!userHistory->mutex.try_lock());
        auto user = userHistory->map.find(message->from->id);
        if (user == userHistory->map.end()) {
            userHistory->mutex.unlock();
            tgBot.getApi().sendMessage(
                    message->chat->id,
                    botSettings["not_authorized"], false, 0, removeKeyboard);
            return;
        }
        userHistory->map.erase(user);
        tgBot.getApi().sendMessage(
                message->chat->id,
                botSettings["not_authorized"], false, 0, removeKeyboard);
        userHistory->mutex.unlock();
        while(!userSettings->mutex.try_lock());
        auto settings = userSettings->map.find(message->from->id);
        if(settings != userSettings->map.end())
            userSettings->map.erase(settings);
        userSettings->mutex.unlock();
    }

    void InvestzBot::handleTickerSetup(
            const Message::Ptr &message,
            const InlineKeyboardMarkup::Ptr &contentTypeKeyboard) {
        // Handle already registered
        std::cout << "ticker setup start" << std::endl;
        auto userID = message->from->id;
        Message::Ptr prevMsg = nullptr;

        while (!userHistory->mutex.try_lock());
        try {
            auto &history = userHistory->map.at(userID);
            if (!history.empty())
                prevMsg = history.back();
        } catch (std::exception &e) {
            std::cout << "ticker setup: " << e.what() << std::endl;
            userHistory->mutex.unlock();
            return;
        }

        if (prevMsg) {
            auto setting = forms::settings::Setting();
            setting.ticker = message->text;
            tgBot.getApi().sendMessage(userID, botSettings["ticker_check"]);
            {
                auto region = "US";
                boost::property_tree::ptree pt;
                auto input = forms::endPointStructs::InputData();
                input.ticker = setting.ticker;
                input.region = region;
                std::stringstream json;

                boost::property_tree::write_json(json, input.getJson());
                auto response = httpClient.sendRequest("web", "80", "/crawler/price", json.str().c_str());
                if (response.result() != http::status::ok){
                    tgBot.getApi().sendMessage(userID, botSettings["ticker_not_found"]);
                    userHistory->mutex.unlock();
                    return;
                }
            }
            setting.user_tg_id = userID;
            while (!userSettings->mutex.try_lock());
            std::cout << "ticker setup: " << std::endl;
            auto settingIT = userSettings->map.find(userID);
            if (settingIT == userSettings->map.end()) {
                std::vector<forms::settings::Setting> settings;
                userSettings->map.insert({userID, settings});
            } else {
                for (auto set: settingIT->second) {
                    std::cout << "try to find ticker.." << std::endl;
                    if (set.ticker != setting.ticker){
                        std::cout << set.ticker << std::endl;
                        continue;
                    }
                    std::cout << setting.ticker << " exists" << std::endl;
                    tgBot.getApi().sendMessage(userID, botSettings["setup_setting_abort"]);
                    userSettings->mutex.unlock();
                    userHistory->mutex.unlock();
                    return;
                }
            }
            userSettings->map.at(userID).push_back(setting);
            userSettings->mutex.unlock();

            while (!userOpen->mutex.try_lock());
            if (userOpen->map.find(userID) == userOpen->map.end())
                userOpen->map.insert({userID, true});
            else
                userOpen->map.at(userID) = true;
            userOpen->mutex.unlock();
            userHistory->mutex.unlock();
            tgBot.getApi().sendMessage(userID, botSettings["choose_info"], false, 0,
                                       contentTypeKeyboard);
        }
    }

    void InvestzBot::handleDemo(const Message::Ptr &message,
                                const ReplyKeyboardRemove::Ptr &removeKeyboard) {
        auto msg = StringTools::split(message->text, ' ');

        if (msg[0] != "get") {
            tgBot.getApi().sendMessage(message->chat->id, botSettings["wrong_format"],
                                       false, message->messageId, removeKeyboard,
                                       "Markdown");
            return;
        }
        auto ticker = msg[1];
        auto region = "US";
        boost::property_tree::ptree pt;
        pt.put("ticker", ticker);
        pt.put("region", region);
        std::stringstream json;
        boost::property_tree::write_json(json, pt, false);
        auto body = json.str();
        *body.rbegin() = '\0';

        std::cout << body << std::endl;
        httpClient.sendRequest("web", "80", "/crawler/news/last", body.c_str());

        auto reply = "Нашлась следующая информация по тикеру `" + msg[1] +
                     "` за последний день " + "\nНовости:\n";
        reply += "\nПрогнозы:\n";
        reply += "\nЦена:\n";
        reply += "\nРиски:\n";
        tgBot.getApi().sendMessage(message->chat->id, reply, false,
                                   message->messageId, removeKeyboard, "Markdown");
    }

}  // namespace serverBot
