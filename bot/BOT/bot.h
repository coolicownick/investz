//
// Created by nickolas on 17.11.2021.
//
#pragma once

#ifndef INVESTZ_SERVERBOT_H
#define INVESTZ_SERVERBOT_H

#include <tgbot/tgbot.h>

#include <map>
#include <string>
#include <vector>
#include <mutex>
#include "forms.h"
#include "sync.http.client.h"

namespace bot {

    template<typename T>
    struct SyncMap {
        std::unordered_map<unsigned int, T> map;
        std::mutex mutex;
    };


    class InvestzBot {
    public:
        InvestzBot();

        void run();
        void sendNotify(std::shared_ptr<forms::Notification> noty);

    private:
        TgBot::Bot tgBot;
        TgBot::TgLongPoll tgLongPoll;
        client::syncHttpClient httpClient;
        std::map<std::string, std::string> botSettings;
        SyncMap<std::vector<TgBot::Message::Ptr>> *userHistory;
        SyncMap<std::vector<forms::settings::Setting>> *userSettings;;
        SyncMap<bool> *userOpen;

        void createOneColumnKeyboard(const std::vector<std::string> &buttonStrings,
                                     TgBot::ReplyKeyboardMarkup::Ptr &kb);

        void createOneColumnInlineKeyboard(
                const std::vector<std::string> &buttonStrings,
                const std::string &callbackName, TgBot::InlineKeyboardMarkup::Ptr &kb);

        bool checkUserExistsCache(const TgBot::Message::Ptr &message);

        void handleCommandNew(const TgBot::Message::Ptr &message);

        void handleDemo(const TgBot::Message::Ptr &message,
                        const TgBot::ReplyKeyboardRemove::Ptr &removeKeyboard);

        void sendSubscribeList(unsigned int userID);

        void handleCommandList(const TgBot::Message::Ptr &message);

        void handleCommandDelete(const TgBot::Message::Ptr &message);

        void handleTickerSetup(
                const TgBot::Message::Ptr &message,
                const TgBot::InlineKeyboardMarkup::Ptr &contentTypeKeyboard);

        void handleUserLogin(const TgBot::Message::Ptr &message, const TgBot::ReplyKeyboardRemove::Ptr &removeKeyboard);

        void
        handleUserLogout(const TgBot::Message::Ptr &message, const TgBot::ReplyKeyboardRemove::Ptr &removeKeyboard);

        std::string printSetting(const int &num, forms::settings::Setting &setting);

        bool sendSubscribeUpdate(unsigned int settingNum, unsigned int userID,
                                 const TgBot::InlineKeyboardMarkup::Ptr &updateKeyboard);

        void handleAnyMessage(
                const TgBot::Message::Ptr &message,
                const TgBot::ReplyKeyboardRemove::Ptr &removeKeyboard,
                const TgBot::InlineKeyboardMarkup::Ptr &contentTypeKeyboard);

        void handleCallbackQuery(
                const TgBot::CallbackQuery::Ptr &query,
                const TgBot::InlineKeyboardMarkup::Ptr &periodKeyboard);

        void handleUpdateCallback(unsigned int &commandNum,const TgBot::CallbackQuery::Ptr &query);

        bool checkUserServer(unsigned int id);
        void loadSettingsServer(unsigned int id);

        std::string generateNewsReply(unsigned int userID,forms::News &news);

        std::string generateConsensusReply(unsigned int userID,forms::Consensus &consensus);

        std::string generatePriceReply(forms::Price &price);

        std::string generatePredictionReply(forms::Prediction &prediction);


    };
}  // namespace serverBot

#endif  // INVESTZ_SERVERBOT_H
