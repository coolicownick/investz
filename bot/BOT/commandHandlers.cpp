//
// Created by nickolas on 27.11.2021.
//

#pragma once

#include "bot.h"

using namespace TgBot;
using forms::settings::Periodic;
using forms::settings::SubscribeTypes;

namespace bot {

    void InvestzBot::handleCommandNew(const Message::Ptr &message) {
        auto botMsg = tgBot.getApi().sendMessage(
                message->chat->id, botSettings["setup_new"], false, message->messageId);
        while (!userHistory->mutex.try_lock());
        if (userHistory->map.find(message->from->id) == userHistory->map.end()) {
            std::cout << "none user" << std::endl;
            userHistory->mutex.unlock();
            return;
        }
        auto &history = userHistory->map.at(message->from->id);
        history.push_back(botMsg);
        std::cout << history.back()->text << std::endl;
        userHistory->mutex.unlock();
    }

    void InvestzBot::handleCommandDelete(const TgBot::Message::Ptr &message) {
        auto botMsg = tgBot.getApi().sendMessage(
                message->chat->id, botSettings["delete"], false, message->messageId);
        while (!userHistory->mutex.try_lock());
        auto &history = userHistory->map.at(message->from->id);
        history.push_back(botMsg);
        userHistory->mutex.unlock();
    }

    bool InvestzBot::sendSubscribeUpdate(unsigned int settingNum, unsigned int userID,
                                         const TgBot::InlineKeyboardMarkup::Ptr &updateKeyboard) {

        while (!userSettings->mutex.try_lock());
        try{
            auto settings = userSettings->map.at(userID);
            userSettings->mutex.unlock();

            auto singleSetting = settings.at(settingNum - 1 );
            auto message = printSetting(-1, singleSetting);
            while(!userHistory->mutex.try_lock());
            auto history = userHistory->map.at(userID);
            userHistory->mutex.unlock();
            tgBot.getApi().sendMessage(userID, message, false, 0, updateKeyboard);
        }catch (std::exception &e){
            tgBot.getApi().sendMessage(userID, botSettings["option_abort"]);
            userSettings->mutex.unlock();
            return false;
        }
        return true;
    }

    void InvestzBot::handleCommandList(const TgBot::Message::Ptr &message) {
        tgBot.getApi().sendMessage(message->chat->id, botSettings["list"]);
        while (!userSettings->mutex.try_lock());
        if (userSettings->map.find(message->chat->id) == userSettings->map.end()) {
            userSettings->mutex.unlock();
            tgBot.getApi().sendMessage(
                    message->chat->id,
                    botSettings["none_subscribe"]);
            return;
        }
        sendSubscribeList(message->chat->id);
        userSettings->mutex.unlock();
    }

    void InvestzBot::sendSubscribeList(unsigned int userID) {

        auto settings = userSettings->map.at(userID);
        if(settings.empty()){
            tgBot.getApi().sendMessage(
                    userID,
                    botSettings["none_subscribe"]);
            return;
        }
        for (size_t i = 0; i < settings.size(); ++i) {
            auto set = settings.at(i);
            auto message = printSetting(i, set);
            tgBot.getApi().sendMessage(userID, message);
        }
    }

    std::string InvestzBot::printSetting(const int &num, forms::settings::Setting &setting) {
        std::string periodStr = "";
        if (setting.period.periodic == Periodic::everyDay) periodStr = "Каждый день";
        if (setting.period.periodic == Periodic::everyWeek) periodStr = "Каждую неделю";
        if (setting.period.periodic == Periodic::everyMonth) periodStr = "Каждый месяц";
        auto settingNumber = num >= 0 ? "/" + std::to_string(num + 1) : "";
        auto msg = settingNumber + " Тикер: " + setting.ticker +
                   "\n Оповещения: " + periodStr + "\n Типы оповещений:";
        std::cout << "set.types[news]: " << setting.types.news
                  << "set.types[consensus]: " << setting.types.consensus
                  << "set.types[price]: " << setting.types.price
                  << "set.types[risk]: " << setting.types.prediction << std::endl;
        if (setting.types.news) msg += "\n \t-Новости";
        if (setting.types.consensus) msg += "\n \t-Консенсунс прогнозы";
        if (setting.types.price) msg += "\n \t-Цена";
        if (setting.types.prediction) msg += "\n \t-Риски";
        msg += "\n";
        return msg;
    }
}  // namespace serverBot
