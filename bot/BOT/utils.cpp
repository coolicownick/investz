//
// Created by nickolas on 27.11.2021.
//

#pragma once

#include "bot.h"
#include <ctime>

using namespace TgBot;
namespace bot {


    void InvestzBot::sendNotify(std::shared_ptr<forms::Notification> noty) {

        while (!userSettings->mutex.try_lock());
        std::cout << "check start" << std::endl;
        try {
            auto userSet = userSettings->map.find(noty->tg_user_id);
            if (userSet == userSettings->map.end()) {
                userSettings->mutex.unlock();
                std::cout << "user doesn't exist" << std::endl;
                return;
            }
            auto settings = userSet->second;
            bool exists = false;
            for (auto setting: settings) {
                if (setting.ticker == noty->ticker) {
                    exists = true;
                    std::cout << "ticker exists" << std::endl;
                    break;
                }
            }
            if (!exists) {
                userSettings->mutex.unlock();
                std::cout << "ticker doesn't exist" << std::endl;
                return;
            }
        } catch (std::exception &e) {
            userSettings->mutex.unlock();
            std::cout << "cannot send notify " << e.what() << std::endl;
        }
        userSettings->mutex.unlock();
        auto reply = "Нашлась следующая информация по тикеру #" + noty->ticker +
                     " за ";
        //set time interval
        if (noty->periodic == forms::settings::everyDay)
            reply += "последний день ";
        if (noty->periodic == forms::settings::everyWeek)
            reply += "последнюю неделю ";
        if (noty->periodic == forms::settings::everyMonth)
            reply += "последний месяц ";
        tgBot.getApi().sendMessage(noty->tg_user_id, reply, false, 0, nullptr, "Markdown");

        reply.erase(reply.begin(), reply.end());
        if (!noty->news.empty()) {
            reply += "\n\t#Новости по #" + noty->ticker + "\n\t\t";
            reply += generateNewsReply(noty->tg_user_id, noty->news.front());
            tgBot.getApi().sendMessage(noty->tg_user_id, reply, false, 0, nullptr, "Markdown");
        }
        reply.erase(reply.begin(), reply.end());
        if (!noty->consensuses.empty()) {
            reply += "\n\t#Консенсус\\_прогнозы по #" + noty->ticker + "\n\t\t";
            reply += generateConsensusReply(noty->tg_user_id, noty->consensuses.front());
            tgBot.getApi().sendMessage(noty->tg_user_id, reply, false, 0, nullptr, "Markdown");
        }
        reply.erase(reply.begin(), reply.end());
        if (noty->prices.ticker != "") {
            reply += "\n\t#Ценa по #" + noty->ticker + "\n\t\t";
            reply += generatePriceReply(noty->prices);
            tgBot.getApi().sendMessage(noty->tg_user_id, reply, false, 0, nullptr, "Markdown");
        }

        reply.erase(reply.begin(), reply.end());
        if (noty->prediction.ticker != "") {
            reply += "\n\t#Прогнозная\\_цена по #" + noty->ticker + "\n\t\t";
            reply += generatePredictionReply(noty->prediction);
            tgBot.getApi().sendMessage(noty->tg_user_id, reply, false, 0, nullptr, "Markdown");
        }
    }

    std::string InvestzBot::generateNewsReply(unsigned int userID, forms::News &news) {
        std::string reply = "";
        reply += "**_" + news.title + "_**\n\t\t";
        reply += "автор: #" + news.author + "\n\t\t";
        reply += news.body + "\n\t\t";
        reply += "источник: " + news.source + "\n\t\t";
        return reply;
    }

    std::string InvestzBot::generateConsensusReply(unsigned int userID, forms::Consensus &consensus) {
        std::string reply = "";
        reply += "**_" + consensus.title + "_**\n\t\t";
        reply += "автор: #" + consensus.author + "\n\t\t";
        reply += consensus.body + "\n\t\t";
        if(consensus.target > 0)
            reply += "прогнозируемая цена: *" + std::to_string(consensus.target) + "*\n\t\t";
        reply += "рекомендация: *" + consensus.recommendation + "*\n\t\t";
        return reply;
    }

    std::string InvestzBot::generatePriceReply(forms::Price &price) {
        std::string reply = "";
        auto time = std::time_t(price.date);
        reply += "**_" + std::string(std::asctime(std::localtime(&time))) + "_**\t\t";
        reply += "*" + std::to_string(price.price) + "*\n\t\t";
        return reply;
    }

    std::string InvestzBot::generatePredictionReply(forms::Prediction &prediction) {
        std::string reply = "";
        reply += "цена завтра: *" + std::to_string(prediction.target) + "*";
        return reply;
    }

    void InvestzBot::createOneColumnKeyboard(
            const std::vector<std::string> &buttonStrings,
            TgBot::ReplyKeyboardMarkup::Ptr &kb) {
        for (size_t i = 0; i < buttonStrings.size(); ++i) {
            std::vector<TgBot::KeyboardButton::Ptr> row;
            TgBot::KeyboardButton::Ptr button(new TgBot::KeyboardButton);
            button->text = buttonStrings[i];
            row.push_back(button);
            kb->keyboard.push_back(row);
        }
    }

    void InvestzBot::createOneColumnInlineKeyboard(
            const std::vector<std::string> &buttonStrings,
            const std::string &callbackName, InlineKeyboardMarkup::Ptr &kb) {
        for (size_t i = 0; i < buttonStrings.size(); ++i) {
            std::vector<InlineKeyboardButton::Ptr> row;
            InlineKeyboardButton::Ptr button(new InlineKeyboardButton);
            button->text = buttonStrings[i];
            button->callbackData = callbackName + "/" + std::to_string(i);
            row.push_back(button);
            kb->inlineKeyboard.push_back(row);
        }
    }

    bool InvestzBot::checkUserExistsCache(const Message::Ptr &message) {

        while (!userHistory->mutex.try_lock());
        auto isNotExist = userHistory->map.find(message->from->id) == userHistory->map.end();
        userHistory->mutex.unlock();
        if (isNotExist) {
            return false;
        }
        return true;
    }

}  // namespace bot
