
#include "bot.h"

#include <exception>
#include <fstream>
#include <stdexcept>
#include <vector>

using namespace TgBot;
namespace bot {

    boost::asio::io_context io_context;

    InvestzBot::InvestzBot()
            : tgBot(std::getenv("BOT_TOKEN")),
              tgLongPoll(tgBot),
              httpClient(io_context),
              userHistory(new SyncMap<std::vector<TgBot::Message::Ptr>>

              ),
              userSettings(new SyncMap<std::vector<forms::settings::Setting>>),
              userOpen(new SyncMap<bool>) {
        userHistory->mutex.unlock();
        userSettings->mutex.unlock();
        userOpen->mutex.unlock();

        std::ifstream settingsFile("settings.toml");
        if (!settingsFile.is_open()) {
            throw std::invalid_argument("Settings file doesn't exists");
        }
        std::string line;
        while (getline(settingsFile, line)) {
            if (line[0] == '#' || line.empty())
                continue;
            auto equalPos = line.find("=");
            std::string name = line.substr(0, equalPos);
            name.erase(std::remove_if(name.begin(), name.end(), isspace), name.end());
            ++equalPos;
            while (line[equalPos] == ' ')
                ++equalPos;
            if (line[equalPos] == '\0' || line[equalPos] == '\n') return;
            auto value = line.substr(equalPos);
            value.erase(std::remove(value.begin(), value.end(), '\"'), value.end());
            botSettings[name] = value;
            std::cout << name << " " << value << '\n';
        }
    }

    void InvestzBot::run() {
        InlineKeyboardMarkup::Ptr periodKeyboard(new InlineKeyboardMarkup);
        InlineKeyboardMarkup::Ptr contentTypeKeyboard(new InlineKeyboardMarkup);
        InlineKeyboardMarkup::Ptr updateKeyboard(new InlineKeyboardMarkup);
        ReplyKeyboardMarkup::Ptr settingsKeyboard(new ReplyKeyboardMarkup);
        ReplyKeyboardMarkup::Ptr loginKeyboard(new ReplyKeyboardMarkup);
        ReplyKeyboardRemove::Ptr removeKeyboard(new ReplyKeyboardRemove);

        createOneColumnKeyboard({"Login", "Logout"}, loginKeyboard);
        createOneColumnInlineKeyboard({"День", "Неделя", "Месяц"}, "period",
                                      periodKeyboard);
        createOneColumnInlineKeyboard(
                {"Новости", "Консенсус прогнозы", "Цена", "Машинные прогнозы", "Готово"}, "content",
                contentTypeKeyboard);
        createOneColumnInlineKeyboard({"Обновить", "Удалить"}, "setting",
                                      updateKeyboard);

        // handle commands:
        tgBot.getEvents().onCommand("menu", [&](Message::Ptr command) {
            if (checkUserExistsCache(command)) {
                tgBot.getApi().sendMessage(command->chat->id, "выберите опцию",
                                           false, 0, loginKeyboard);
                return;
            }
            tgBot.getApi().sendMessage(command->chat->id, botSettings["greetings"],
                                       false, 0, loginKeyboard);
        });

        tgBot.getEvents().onCommand("new", [&](Message::Ptr command) {
            std::cout << "exists : " << (checkUserExistsCache(command) ? "true" : "false") << std::endl;
            std::cout << "debug new" << std::endl;
            if (!checkUserExistsCache(command)) {
                tgBot.getApi().sendMessage(
                        command->chat->id,
                        botSettings["not_authorized"]);
                return;
            }
            handleCommandNew(command);
        });
        tgBot.getEvents().onCommand("delete", [&](Message::Ptr command) {
            if (!checkUserExistsCache(command)) {
                tgBot.getApi().sendMessage(
                        command->chat->id,
                        botSettings["not_authorized"]);
                return;
            }
            handleCommandDelete(command);
        });
        tgBot.getEvents().onCommand("list", [&](Message::Ptr command) {
            if (!checkUserExistsCache(command)) {
                tgBot.getApi().sendMessage(
                        command->chat->id,
                        botSettings["not_authorized"]);
                return;
            }
            handleCommandList(command);
        });
        // TODO create msg with decriprtion

        tgBot.getEvents().onAnyMessage([&](Message::Ptr message) {

            if (StringTools::startsWith(message->text, "/new") ||
                StringTools::startsWith(message->text, "/menu") ||
                StringTools::startsWith(message->text, "/list")){

                std::cout << "it's command" << std::endl;
                return;
            }

            if (StringTools::startsWith(message->text, "/") &&
                checkUserExistsCache(message)) {
                message->text.erase(message->text.begin());
                std::cout << "try check" << message->text << std::endl;
                if(message->text.empty())
                    return;
                auto num = message->text;
                try {
                    auto req = boost::lexical_cast<unsigned int>(num.c_str());
                    if (sendSubscribeUpdate(req, message->from->id, updateKeyboard)){
                        while(!userHistory->mutex.try_lock());
                        userHistory->map.at(message->from->id).push_back(message);
                        userHistory->mutex.unlock();
                    }
                    return;
                } catch (boost::bad_lexical_cast const &e) {
                    std::cout << e.what() << " printed: " << num << std::endl;
                    userHistory->mutex.unlock();
                    return;
                }

            }


            if (StringTools::startsWith(message->text, "get")) {
                handleDemo(message, removeKeyboard);
                return;
            }
            std::cout << "debug handle any message "<< message->text << std::endl;
            std::cout << "exists s : " << (checkUserExistsCache(message) ? "true" : "false") << std::endl;
            handleAnyMessage(message, removeKeyboard, contentTypeKeyboard);
        });
        tgBot.getEvents().onCallbackQuery([&](CallbackQuery::Ptr query) {
            handleCallbackQuery(query, periodKeyboard);
        });


        try {
            std::cout << "Bot username: " << tgBot.getApi().getMe()->username
                      << std::endl;
            tgBot.getApi().deleteWebhook();
            while (true) {
                std::cout << "Long poll started" << std::endl;
                tgLongPoll.start();
            }

        } catch (std::exception &e) {
            std::cout << "error: " << e.what() << std::endl;
        }


    }


}  // namespace serverBot
