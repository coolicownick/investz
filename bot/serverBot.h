//
// Created by nickolas on 10.12.2021.
//

#ifndef BOT_SERVER_H
#define BOT_SERVER_H

#include "BOT/bot.h"
#include <string>
#include <vector>
#include <mutex>
#include "net/server/async.http.server.h"

namespace serverBot {
    class Investz : public server::asyncHttpServer {
    public:
        explicit Investz(const std::string &address, const std::string &port,
                           std::size_t thread_pool_size = get_nprocs());
        static bot::InvestzBot investBot;
    private:
    };

    server::Response notificationHandler(const server::Request &request);

}


#endif //BOT_SERVER_H
