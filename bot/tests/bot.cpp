//
// Created by nickolas on 19.10.2021.
//

#include <gtest/gtest.h>
#include "../BOT/bot.h"

TEST(_BOT_TEST, _Assert_Bot) {
    std::string token = "124saa21dzx";
    TgBot::Bot bot(token);
    TgBot::TgLongPoll longPoll(bot);
    longPoll.start();
}
