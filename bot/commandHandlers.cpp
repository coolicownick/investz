//
// Created by nickolas on 27.11.2021.
//

#include "serverBot.h"

using namespace TgBot;
using forms::settings::Periodic;
using forms::settings::SubscribeTypes;

namespace serverBot {

    void InvestzBot::handleCommandNew(const Message::Ptr &message) {
        auto botMsg = tgBot.getApi().sendMessage(
                message->chat->id, botSettings["setup_new"], false, message->messageId);
        std::cout << "debug:" << std::endl;
        if (userHistory->find(message->from->id) == userHistory->end()) {
            std::cout << "none user" << std::endl;
            return;
        }
        auto &history = userHistory->at(message->from->id);
        history.push_back(botMsg);
        std::cout << history.back()->text << std::endl;
    }

    void InvestzBot::handleCommandList(const TgBot::Message::Ptr &message) {
        tgBot.getApi().sendMessage(message->chat->id, botSettings["list"]);
        if (userSettings->find(message->chat->id) == userSettings->end()) {
            tgBot.getApi().sendMessage(
                    message->chat->id,
                    botSettings["none_subscribe"]);
            return;
        }
        sendSubscribeList(message->chat->id);
    }

    void InvestzBot::handleCommandDelete(const TgBot::Message::Ptr &message) {
        auto botMsg = tgBot.getApi().sendMessage(
                message->chat->id, botSettings["delete"], false, message->messageId);
        auto &history = userHistory->at(message->from->id);
        history.push_back(botMsg);
    }

    void InvestzBot::sendSubscribeList(int userID){
        auto settings = userSettings->at(userID);
        for (size_t i = 0; i < settings.size(); ++i) {
            auto set = settings.at(i);
            std::string periodStr = "";
            if (set.period.periodic == Periodic::everyDay) periodStr = "Каждый день";
            if (set.period.periodic == Periodic::everyWeek) periodStr = "Каждую неделю";
            if (set.period.periodic == Periodic::everyMonth) periodStr = "Каждый месяц";

            auto msg = "/" + std::to_string(i + 1) + " Тикер: " + set.ticker +
                       "\n Оповещения: " + periodStr + "\n Типы оповещений:";
            std::cout << "set.types[news]: " << set.types.news
                      << "set.types[consensus]: " << set.types.consensus
                      << "set.types[price]: " << set.types.price
                      << "set.types[risk]: " << set.types.prediction << std::endl;
            if (set.types.news) msg += "\n \t-Новости";
            if (set.types.consensus) msg += "\n \t-Консенсунс прогнозы";
            if (set.types.price) msg += "\n \t-Цена";
            if (set.types.prediction) msg += "\n \t-Риски";
            msg += "\n";
            tgBot.getApi().sendMessage(userID, msg);
        }
    }
}  // namespace serverBot
