#include <tgbot/tgbot.h>

#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <string>
#include <vector>

#include "serverBot.h"

int main() {


    auto _host = "0.0.0.0";
    auto _port = "8040";

    auto server = std::make_shared<serverBot::Investz>(_host,_port,2);
    server->run();
    return 0;
}