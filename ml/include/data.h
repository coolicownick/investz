
#ifndef ML_DATA_H
#define ML_DATA_H

#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include <stdlib.h>
#include <cmath>
#include <Eigen/Dense>

#include <boost/algorithm/string.hpp>

class DATA {
//Данные из нашего файла
    std::string dataset;
    // Разделитель данных
    std::string delimiter;
public:
    //Простой конструктор
    DATA(std::string data, std::string separator) : dataset(data), delimiter(separator)
    {}
    //Функция чтения данных из файла и запись в векторном векторе строк
    std::vector<std::vector<std::string>> read_csv();
    //Делаем матрицу из вектора векторов и переводим их в тип float
    Eigen::MatrixXd csvtoEigen(std::vector<std::vector<std::string>> dataset, int rows, int cols);
    //Так как могут быть величины разной величины, то надо провести нормализацию
    Eigen::MatrixXd Normalize(Eigen::MatrixXd data, bool normalizeTarget);
    //Возвращаем среднее значение каждого столбца
    Eigen::MatrixXd Mean(Eigen::MatrixXd data);
    //Стандартное отклонение
    Eigen::MatrixXd Std(Eigen::MatrixXd data);
    //Разделение данных на тестовую и обучающую выборку
    std::tuple<Eigen::MatrixXd,Eigen::MatrixXd,Eigen::MatrixXd,Eigen::MatrixXd> TrainTestSplit(
            Eigen::MatrixXd data, float train_size);
    void Vectortofile(std::vector<float> vector, std::string filename);
    void EigentoFile(Eigen::MatrixXd data, std::string filename);
};


#endif //ML_DATA_H
