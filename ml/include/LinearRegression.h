//
// Created by Александр on 17.11.2021.
//

#ifndef ML_LINEARREGRESSION_H
#define ML_LINEARREGRESSION_H

#include <Eigen/Dense>
#include <iostream>
#include <string.h>
#include <vector>
#include <stdlib.h>
#include <cmath>
#include "async.http.server.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>



namespace linear {
    server::Response HandleGetPrice(const server::Request &request);

    class LinearHttpServer : public server::asyncHttpServer {
    public:
        explicit LinearHttpServer(const std::string &address, const std::string &port,
                                  std::size_t thread_pool_size = 1)
                : server::asyncHttpServer(address, port,
                                          thread_pool_size) {
            request_router.addHandler("/price/prediction", HandleGetPrice);
        }
    };

    class Linear {
    private:
        //  Heap<Subscriber>* m_subscribers;
        //  Message m_generatedMessage;

        // public:
        //  const Subscriber* getNextSubscriber() const {
        //    const Subscriber* _subs = &m_subscribers->top();
        //    return _subs;
        //  }
        //  void init(Heap<Subscriber>* _data) { m_subscribers = _data; };
        //  Settings waitSettingsFromTelegram(const Subscriber*) const {
        //    return Settings();
        //  }
        //  Data generateDataForML(const Subscriber*) { return Data(); }
        //  bool sendDataToML(const Data) const { return true; }
        //  Data waitResponseFromML() const { return Data(); }
        //  Message generateSubscriberMsg(const Subscriber*) const { return Message(); }
        //  bool pushMessageToDB(Message*) const { return true; }
        //  Data findContent(Settings /*, stocksComponents::StocksAPI**/) {
        //    return Data();
        //  };

    protected:
        explicit Linear(const Linear &) = delete;

        explicit Linear(Linear &&) = delete;

        Linear &operator=(const Linear &) = delete;

        Linear &operator=(Linear &&) = delete;

    private:
        Linear() {};

        ~Linear() {};

        //  friend Singleton<Crawler>;
        // stocksComponents::StocksAPI* getAccessToStocksAPI();
        void findNews() {};

        void findConsensuns() {};

        void getPrice() {};
        // Struct* getSubscriberSettings(Subscriber*);
    };

    struct coefs {
        std::string day_1;
        std::string day_2;
        std::string day_3;
        std::string day_4;
        std::string day_5;
        std::string day_6;
        std::string day_7;
        friend std::istream &operator>>(std::istream &in, linear::coefs &obj);

        friend std::ostream &operator<<(std::ostream &in, const linear::coefs &obj);
    };

    std::istream &operator>>(std::istream &in, linear::coefs &obj);

    std::ostream &operator<<(std::ostream &out, const linear::coefs &obj);

};

class LinearRegression {
public:
    LinearRegression() {}
//Надо найти зависимость y = kx + b, где х - это наши данные, к - это коэффициент для каждого столбца,
//b - это необходимая константа.
//Ordinary Least Squares (OLS)
    float OLS_Cost(Eigen::MatrixXd X, Eigen::MatrixXd y, Eigen::MatrixXd theta);
//Алгоритм оптимизации через градиент
    std::tuple<Eigen::VectorXd,std::vector<float>> GradientDescent(Eigen::MatrixXd X,
                                                                   Eigen::MatrixXd y, Eigen::VectorXd theta,
                                                                   float alpha, int iters);

    float RSquared(Eigen::MatrixXd y, Eigen::MatrixXd y_hat);
};

#endif //ML_LINEARREGRESSION_H
