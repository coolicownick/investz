#!/bin/bash

rm -rf build && mkdir build && cd build

cp ../src/Data.csv .
cmake ..
make -j4
/ML &

pid="$!"

while true
do
  if [ -d /proc/$pid ]; then
    wait "$pid"
    sleep 1
  else
    make -j4
    ./ML &
    pid="$!"
  fi
done
