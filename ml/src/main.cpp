#include <iostream>
#include "LinearRegression.h"
#include "forms.h"

constexpr auto __host = "0.0.0.0";
constexpr auto __port = "8070";

int main() {
    auto linear = std::make_shared<linear::LinearHttpServer>(__host, __port);
    linear->run();
    return EXIT_SUCCESS;
}

