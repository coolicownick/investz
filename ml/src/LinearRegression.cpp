#include "LinearRegression.h"
#include "data.h"
#include "forms.h"

server::Response linear::HandleGetPrice(const server::Request& request) {
    server::Response response{http::status::bad_request, request.version()};
    std::stringstream ss(request.body());
    std::cout << request << std::endl;
    std::stringstream out;
    boost::property_tree::ptree pt;
    boost::property_tree::read_json(ss, pt);

    auto price_data = forms::DataForML(pt);
    std::cout << &price_data << ' ' << price_data.open << ' ' << price_data.price << ' ' << price_data.low << ' ' << price_data.close << std::endl;
    std::vector<float>  X;
    X.push_back(price_data.open);
    X.push_back(price_data.price);
    X.push_back(price_data.low);
    X.push_back(price_data.close);

    std::vector<std::vector<float>> dataStr;
    dataStr.push_back(X);

    float tetha_1 = 3.14797;
    float tetha_2 = 3.15914;
    float tetha_3 = 3.16016;
    float tetha_4 = 3.17297;

    std::vector<float>  theta_tmp;

    theta_tmp.push_back(tetha_1);
    theta_tmp.push_back(tetha_2);
    theta_tmp.push_back(tetha_3);
    theta_tmp.push_back(tetha_4);

    std::vector<std::vector<float>> Theta0;
    Theta0.push_back(theta_tmp);

    Eigen::MatrixXd theta(Theta0[0].size(), Theta0.size());

    for (int i = 0; i < Theta0.size(); i++) {
        for (int j = 0; j < Theta0[0].size(); j++) {
            theta(j, i) = Theta0[i][j];
        }
    }

    Eigen::MatrixXd mat_1(dataStr[0].size(), dataStr.size());
    for (int i = 0; i < dataStr.size(); i++) {
        for (int j = 0; j < dataStr[0].size(); j++) {
            mat_1(j, i) = dataStr[i][j];
        }
    }

    mat_1.transposeInPlace();
    Eigen::MatrixXd temp = mat_1 * theta * 0.08;
    float price_tomorrow = temp.coeff(0, 0);

    boost::property_tree::ptree pt_1;

    pt_1.put("prediction", price_tomorrow);

    std::stringstream ss_1;
    boost::property_tree::write_json(ss_1, pt_1);
    response.result(http::status::ok);
    response.body() = ss_1.str();
    return response;
}

//Ordinary Least Squares (OLS) - представляет собой тип линейной наименьших квадратов метода оценки
//неизвестных параметров в линейной регрессии модели. OLS выбирает параметры линейной функции набора
//независимых переменных по принципу наименьших квадратов : минимизируя сумму квадратов различий между
//наблюдаемой зависимой переменной (значения наблюдаемой переменной) в данном наборе данных и
// предсказанными линейной функцией независимой переменной
float LinearRegression::OLS_Cost(Eigen::MatrixXd X, Eigen::MatrixXd y, Eigen::MatrixXd theta){

    Eigen::MatrixXd inner = pow(((X*theta)-y).array(),2);

    return (inner.sum()/(2*X.rows()));
}
//Градиентный спуск ищет возрастание разницы через частную производную и идёт в другую сторону
std::tuple<Eigen::VectorXd,std::vector<float>> LinearRegression::GradientDescent(Eigen::MatrixXd X,
                                                                                 Eigen::MatrixXd y,
                                                                                 Eigen::VectorXd theta,
                                                                                 float alpha, int iters){
    //alpha - коэффициент, к которому мы стремимся
    //theta будет обновляться
    //iters - количество итераций, необходимых для того, чтобы alpha достиг нудного предела.
    Eigen::MatrixXd temp = theta;

    int parameters = theta.rows();

    std::vector<float> cost;
    cost.push_back(OLS_Cost(X,y,theta));
    //Матрица-заполнитель для theta
    for(int i=0; i<iters; ++i){
        //Смотрим ошибку и далее обновляем веса
        Eigen::MatrixXd error = X*theta - y;
        for(int j=0; j<parameters; ++j){
            Eigen::MatrixXd X_i = X.col(j);
            Eigen::MatrixXd term = error.cwiseProduct(X_i);
            temp(j,0) = theta(j,0) - ((alpha/X.rows())*term.sum());
        }
        theta = temp;
        cost.push_back(OLS_Cost(X,y,theta));
    }

    return std::make_tuple(theta,cost);
}

float LinearRegression::RSquared(Eigen::MatrixXd y, Eigen::MatrixXd y_hat){
    auto num = pow((y-y_hat).array(),2).sum();
    auto den = pow(y.array()-y.mean(),2).sum();

    return 1 - num/den;
}
