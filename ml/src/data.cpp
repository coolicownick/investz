#include "data.h"

std::vector<std::vector<std::string>> DATA::read_csv(){

    std::ifstream file(dataset);
    std::vector<std::vector<std::string>> dataString;
//Повторяем каждую строку и разделяем ее
    std::string line = "";

    while(getline(file,line)){
        std::vector<std::string> vec;
        boost::algorithm::split(vec,line,boost::is_any_of(delimiter));
        dataString.push_back(vec);
    }

    file.close();

    return dataString;
}
//Заполняем нашу матрицу данным из вектора переводя числа из
Eigen::MatrixXd DATA::csvtoEigen(std::vector<std::vector<std::string>> dataset, int rows, int cols) {
    Eigen::MatrixXd mat(cols, rows);
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            mat(j, i) = atof(dataset[i][j].c_str());
        }
    }
    return mat.transpose();
}

Eigen::MatrixXd DATA::Normalize(Eigen::MatrixXd data, bool normalizeTarget){

    Eigen::MatrixXd dataNorm;
    if(normalizeTarget == true) {
        dataNorm = data;
    } else {
        dataNorm = data.leftCols(data.cols() - 1);
    }
    //Среднее по всем столбцам
    Eigen::MatrixXd mean = Mean(dataNorm);
    //Разница каждого наблюдения с средним

    int rows = dataNorm.rows();
    int cols = dataNorm.cols();

    Eigen::MatrixXd scaled_data(rows, cols);

    std::cout << dataNorm(0, 0) - mean(0) << "sfdbf" << std::endl;
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            scaled_data(i, j) = dataNorm(i, j) - mean(j);
        }
    }

    Eigen::MatrixXd std = Std(scaled_data);

    //Применяем нормализацию, то есть делим отклонений на стандартное отклонение

    Eigen::MatrixXd norm(rows, cols);
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            norm(i, j) = scaled_data(i, j) / std(j);
        }
    }

    return norm;
}
//Eigen::MatrixXd DATA::Mean(Eigen::MatrixXd data) -> decltype(data.colwise().mean())
Eigen::MatrixXd DATA::Mean(Eigen::MatrixXd data) {
    return data.colwise().mean();
}

Eigen::MatrixXd DATA::Std(Eigen::MatrixXd data) {
    return ((data.array().square().colwise().sum())/(data.rows()-1)).sqrt();
}
//
std::tuple<Eigen::MatrixXd,Eigen::MatrixXd,Eigen::MatrixXd,Eigen::MatrixXd>
DATA::TrainTestSplit(Eigen::MatrixXd data, float train_size){

    int rows = data.rows();
    //Указываем сколько строк занимает обучающая выборка
    int train_rows = round(train_size*rows);
    //Указываем сколько столбцов занимает тестовая выборка
    int test_rows = rows - train_rows;
    //Обучающая выборка займет верхний строки
    Eigen::MatrixXd train = data.topRows(train_rows);
    //Указываем какие столбцы относятся к обучающей, а какие являются ответом
    Eigen::MatrixXd X_train = train.leftCols(data.cols()-1);
    Eigen::MatrixXd y_train = train.rightCols(1);
    //Тоже самое и для тестовой выборки
    Eigen::MatrixXd test = data.bottomRows(test_rows);

    Eigen::MatrixXd X_test = test.leftCols(data.cols()-1);
    Eigen::MatrixXd y_test = test.rightCols(1);
    //Вернем разделенные данные
    return std::make_tuple(X_train, y_train, X_test, y_test);
}

void DATA::Vectortofile(std::vector<float> vector, std::string filename){
    std::ofstream output_file(filename);
    std::ostream_iterator<float> output_iterator(output_file, "\n");
    std::copy(vector.begin(), vector.end(), output_iterator);
}

void DATA::EigentoFile(Eigen::MatrixXd data, std::string filename){
    std::ofstream output_file(filename);
    if(output_file.is_open()){
        output_file << data << "\n";
    }
}