#!/bin/bash

rm -rf build && mkdir build && cd build
cmake ..
make -j
./crawler &
pid="$!"

while true
do
  if [ -d /proc/$pid ]; then
    wait "$pid"
    sleep 1
  else
    make -j
    ./crawler &
    pid="$!"
  fi
done
