#include "crawler.h"

#include <functional>

#include "forms.h"
#include "tools.h"
using namespace forms::constants;
using namespace forms::endPointStructs;
using namespace yahooFinance::targets;
using namespace client;
using boost::beast::buffers_to_string;
using boost::property_tree::ptree;
using boost::property_tree::read_json;
using boost::property_tree::write_json;
using std::stringstream;
using badJsonData = boost::wrapexcept<boost::property_tree::json_parser::json_parser_error>;


namespace yahooFinance {
    constexpr auto rapidApiHost = "yh-finance.p.rapidapi.com";
    constexpr auto rapidApiKey_field = "x-rapidapi-key";
    constexpr auto rapidApiKey_token =
            "91ee04c559msheea85d4f346a40bp12e460jsn2dd9fb1c1cf7";
}// namespace yahooFinance

namespace crawler {
    boost::asio::io_context crawler_io_context;

    server::Response createDefaultResponse(http::status status, const std::string& msg) {
        server::Response response;
        ptree json;
        json.put(messageField, msg);
        response.body() = tools::toString(json);
        response.result(status);
        return response;
    }

    server::Response finishedResponseFromJSON(const ptree& resultJson, const server::Request& request) {
        server::Response response{http::status::ok, request.version()};
        response.set(http::field::content_type, defaultTypeIsJson);
        response.body() = tools::toString(resultJson);
        return response;
    }

    ptree fillNews(const ptree& yahooNews, syncHttpSSLClient& yahooFinanceClient, const InputData& data) {
        ptree news;
        size_t count{0};
        for (auto newsIt{yahooNews.begin()}; newsIt != yahooNews.end() && count < maxNews; newsIt++, count++) {
            auto oneNewsJson = newsIt->second;
            auto response = yahooFinanceClient.sendRequest(yahooFinance::rapidApiHost,
                                                           httpsPort,
                                                           NewsEndPoint().getDetails(oneNewsJson.get<std::string>(uuidField), data.region).c_str());
            ptree newsDetails = tools::toJson(buffers_to_string(response.body().data())).get_child(dataField);
            auto content = newsDetails.get_child(contentsField).begin()->second.get_child(contentField);
            auto image = content.get<std::string>(pathToImage, defaultStringField);
            auto summary = content.get<std::string>(summaryField, defaultStringField);

            forms::News oneNews;
            oneNews.ticker = data.ticker;
            oneNews.notify_id = data.notify_id;
            oneNews.title = std::move(oneNewsJson.get<std::string>(titleField, defaultStringField));
            oneNews.publishedDate = oneNewsJson.get<unsigned long>(providerPublishTimeField, 0);
            oneNews.author = std::move(oneNewsJson.get<std::string>(publisherField, defaultStringField));
            oneNews.source = std::move(oneNewsJson.get<std::string>(linkField, defaultStringField));
            oneNews.image = std::move(image);
            oneNews.body = std::move(summary);
            std::cout << "NOTIFY IS = " << oneNews.notify_id;
            news.push_back(std::make_pair(defaultStringField, std::move(oneNews.getJson())));
        }
        return news;
    }

    ptree fillConsensus(const ptree& yahooResultConsensus, syncHttpSSLClient& yahooFinanceClient, const InputData& data) {
        size_t count{0};
        ptree consensus;
        auto yahooConsensus = yahooResultConsensus.get_child(reportsField);
        auto recommendations = yahooResultConsensus.get_child(info_RecommendationField);
        for (auto consensusIt{yahooConsensus.begin()}; consensusIt != yahooConsensus.end() && count < maxConsensus; consensusIt++, count++) {
            auto oneConsensusJson = consensusIt->second;
            forms::Consensus oneConsensus;
            oneConsensus.ticker = data.ticker;
            oneConsensus.notify_id = data.notify_id;
            oneConsensus.title = oneConsensusJson.get<std::string>(titleField, defaultStringField);
            oneConsensus.author = oneConsensusJson.get<std::string>(providerField, defaultStringField);
            oneConsensus.body = oneConsensusJson.get<std::string>(summaryField, defaultStringField);
            auto fieldWithTime = oneConsensusJson.get<std::string>(idField, defaultStringField);
            auto time = fieldWithTime.substr(fieldWithTime.rfind('_') + 1);
            oneConsensus.publishedDate = std::atof(time.c_str());
            oneConsensus.target = recommendations.get<float>(targetPriceField, 0);
            oneConsensus.recommendation = recommendations.get<std::string>(ratingField, defaultStringField);
            std::cout << "NOTIFY IS = " << oneConsensus.notify_id;
            consensus.push_back(std::make_pair(defaultStringField, std::move(oneConsensus.getJson())));
        }
        return consensus;
    }

    ptree fillDataForML(const ptree& yahooFinancialsData, syncHttpSSLClient& yahooFinanceClient, const InputData& data) {
        forms::DataForML dataForMl;
        dataForMl.ticker = data.ticker;
        dataForMl.price = yahooFinancialsData.get<float>(priceRegularMarketOpenRawField, 0);
        dataForMl.low = yahooFinancialsData.get<float>(regularMarketDayLowRawField, 0);
        ptree summaryDetailData = yahooFinancialsData.get_child(summaryDetailField);
        dataForMl.open = summaryDetailData.get<float>(openRawField, 0);
        dataForMl.volume = summaryDetailData.get<float>(tenDaysAverageVolumeRawField, 0);
        dataForMl.close = summaryDetailData.get<float>(previousCloseRawField, 0);
        dataForMl.dividends = summaryDetailData.get<float>(trailingAnnualDividendRateRawField, 0);
        return dataForMl.getJson();
    }

    ptree fillPrice(const ptree& yahooPriceData, syncHttpSSLClient& yahooFinanceClient, const InputData& data) {
        forms::Price price;
        price.ticker = data.ticker;
        price.notify_id = data.notify_id;
        price.date = yahooPriceData.get<long>(regularMarketTimeField, 0);
        price.price = yahooPriceData.get<float>(regularMarketPriceRawField, 0);
        return price.getJson();
    }

    server::Response GetDataFromYahoo(const server::Request& request, const InputData& data, const std::string& endPoint, const std::string& field, const std::function<ptree(const ptree&, syncHttpSSLClient&, const InputData&)>& func) {
        //        auto data = InputData(tools::toJson(request.body()));
        bool invalidRegion = (yahooFinance::Regions.find(data.region) == yahooFinance::Regions.end());
        if (invalidRegion) {
            return createDefaultResponse(http::status::internal_server_error, forms::errors::invalidRegion);
        }
        syncHttpSSLClient client(crawler_io_context);
        client.addHeaders(yahooFinance::rapidApiKey_field, yahooFinance::rapidApiKey_token);
        //diff
        std::cout << "-->request:\n"
                  << request << std::endl;

        auto yahooResponse = client.sendRequest(yahooFinance::rapidApiHost,
                                                httpsPort,
                                                endPoint.c_str());
        if (yahooResponse.result() != http::status::ok) {
            return createDefaultResponse(http::status::internal_server_error, forms::errors::invalidTicker);
        }
        auto yahooData = tools::toJson(buffers_to_string(yahooResponse.body().data())).get_child(field);
        if (yahooData.empty()) {
            return createDefaultResponse(http::status::internal_server_error, forms::errors::invalidTicker);
        }
        ptree childWithData{func(yahooData, client, data)};
        return finishedResponseFromJSON(childWithData, request);
    }

    server::Response HandleGetNews(const server::Request& request) {
        try {
            auto inputData = InputData(tools::toJson(request.body()));
            auto response = GetDataFromYahoo(request, inputData, EndPoint().autoComplete(inputData.ticker, inputData.region), newsField, fillNews);
            if (response.result() != http::status::ok) {
                return response;
            }
            auto data = tools::toJson(response.body());
            ptree resultJson;
            resultJson.add_child(newsField, data);
            return finishedResponseFromJSON(resultJson, request);
        } catch (const std::exception& error) {
            return createDefaultResponse(http::status::not_found, error.what());
        }
    }

    server::Response HandleGetConsensus(const server::Request& request) {
        try {
            auto inputData = InputData(tools::toJson(request.body()));
            auto response = GetDataFromYahoo(request, inputData, StockEndPoint().getInsights(inputData.ticker), resultField, fillConsensus);
            if (response.result() != http::status::ok) {
                return response;
            }
            auto data = tools::toJson(response.body());
            ptree resultJson;
            resultJson.add_child(consensusField, data);
            return finishedResponseFromJSON(resultJson, request);
        } catch (const std::exception& error) {
            return createDefaultResponse(http::status::not_found, error.what());
        }
    }

    server::Response HandlePrepareData(const server::Request& request) {
        try {
            auto inputData = InputData(tools::toJson(request.body()));
            inputData.region = defaulRegionIsUS;
            auto response = GetDataFromYahoo(request, inputData,
                                             StockEndPoint().getFinancials(inputData.ticker, inputData.region),
                                             defaultStringField, fillDataForML);
            write_json(std::cout, tools::toJson(response.body()));
            if (response.result() != http::status::ok) {
                return response;
            }
            auto data = tools::toJson(response.body());
            return finishedResponseFromJSON(data, request);
        } catch (const std::exception& error) {
            return createDefaultResponse(http::status::not_found, error.what());
        }
    }

    server::Response HandleGetPrice(const server::Request& request) {
        try {
            auto inputData = InputData(tools::toJson(request.body()));
            inputData.region = defaulRegionIsUS;
            auto response = GetDataFromYahoo(request, inputData,
                                             StockEndPoint().getFinancials(inputData.ticker, inputData.region),
                                             priceField, fillPrice);
            if (response.result() != http::status::ok) {
                return response;
            }
            auto data = tools::toJson(response.body());
            ptree resultJson;
            resultJson.add_child(priceField, data);
            return finishedResponseFromJSON(resultJson, request);
        } catch (const std::exception& error) {
            return createDefaultResponse(http::status::not_found, error.what());
        }
    }


    server::Response toTarget(const server::Request& request, syncHttpClient& client, const std::string& bodyForSending, const http::verb& method, const std::string& target) {
        std::cout << "-->request to:\n"
                  << nginx << ' ' << nginxPort << ' ' << target << ' ' << bodyForSending.c_str();
        server::Response res{http::status::internal_server_error, request.version()};
        auto responseFromClient = client.sendRequest(nginx, method, nginxPort, target.c_str(), bodyForSending.c_str());
        auto responseFromServer{server::Response()};
        responseFromServer.body() = buffers_to_string(responseFromClient.body().data());
        responseFromServer.result(responseFromClient.result());
        return responseFromServer;
    }

    server::Response HandleWrite(const server::Request& request) {
        typedef server::Response step;
        try {
            ptree json{tools::toJson(request.body())};
            json = json.get_child(crawlerRequestField);
            forms::CrawlerRequest req(json);
            //write_json(std::cout, req.getJson());
            syncHttpClient client(crawler_io_context);
            InputData data;
            data.region = defaulRegionIsUS;
            data.ticker = req.ticker;
            data.notify_id = req.notify_id;
            bool isNeedNews = req.types.news;
            auto bodyForSending = tools::toString(data.getJson());
            if (isNeedNews) {
                step getNews = toTarget(request, client, bodyForSending, http::verb::get, crawler_newslast_target);
                if (getNews.result() != http::status::ok) {
                    std::cout << getNews << std::endl;
//                    return getNews;
                }
                step setNews = toTarget(request, client, getNews.body(), http::verb::post, api_setnews_target);
                if (setNews.result() != http::status::ok) {
                    std::cout << setNews << std::endl;
//                    return setNews;
                }
            }
            bool isNeedConsensus = req.types.consensus;
            if (isNeedConsensus) {
                step getConsensus = toTarget(request, client, bodyForSending, http::verb::get, crawler_consensus_target);
                //std::cout << getConsensus << std::endl;
                if (getConsensus.result() != http::status::ok) {
                    std::cout << getConsensus << std::endl;
//                    return getConsensus;
                }
                step setConsensus = toTarget(request, client, getConsensus.body(), http::verb::post, api_setconsensus_target);
                if (setConsensus.result() != http::status::ok) {
                    std::cout << setConsensus << std::endl;
//                    return setConsensus;
                }
            }
            bool isNeedPrediction = req.types.prediction;
            if (isNeedPrediction) {
                step getDataForMl = toTarget(request, client, bodyForSending, http::verb::get, crawler_prepareDataForMl_target);
                step getPredictionFromMl = toTarget(request, client, getDataForMl.body(), http::verb::get, ml_pricePridection_target);
                forms::Prediction prediction;
                prediction.ticker = data.ticker;
                prediction.notify_id = data.notify_id;
                prediction.target = tools::toJson(getPredictionFromMl.body()).get<float>(predictionField, 0);
                step setPrediction = toTarget(request, client, tools::toString(prediction.getJson()), http::verb::post, api_setprediction_target);
            }
            bool isNeedPrice = req.types.price;
            if (isNeedPrice) {
                step getPrice = toTarget(request, client, bodyForSending, http::verb::get, crawler_getprice_target);
                step setPrice = toTarget(request, client, getPrice.body(), http::verb::post, api_setprice_target);
            }
        } catch (const std::exception& error) {
            return createDefaultResponse(http::status::internal_server_error, error.what());
        }
        //    if (isNeedNews) {
        //        getNews(request, client, tools::toString(data.getJson()));
        //    }
        //    server::Response res{http::status::ok, request.version()};
        return createDefaultResponse(http::status::ok, forms::constants::OkMsg);
    }
}// namespace crawler