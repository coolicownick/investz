#include "yahooFinance.h"

#include <algorithm>
#include <utility>

#include "boost/algorithm/string/replace.hpp"
namespace yahooFinance::targets {
    constexpr auto spacer = "%20";
    EndPoint::EndPoint(std::string _prefix) : m_prefix(std::move(_prefix)){};
    EndPoint::EndPoint() : EndPoint("/auto-complete"){};
    std::string EndPoint::autoComplete(const std::string& ticker,
                                       const std::string& region) {
        std::string tempTicker{ticker}, tempRegion{region};
        boost::replace_all(tempTicker, " ", spacer);
        boost::replace_all(tempRegion, " ", spacer);
        std::string requiredParameters{"q="};
        std::string q = std::move(tempTicker);
        std::string optionalParameters{"region="};
        std::string target{m_prefix + '?' + std::move(requiredParameters) + std::move(q) + '&' +
                           std::move(optionalParameters) + std::move(tempRegion)};
        return target;
    }

    MarketEndPoint::MarketEndPoint() : EndPoint("/market"){};
    std::string MarketEndPoint::getQuotes(std::string region, std::set<std::string>& symbols) const {
        std::string symbolsStr;
        for (auto& symbol : symbols) {
            symbolsStr += symbol + "%2C";
        }
        std::string requiredParameters =
                "region=" + region + "&" + "symbols=" + symbolsStr;
        std::string target = m_prefix + "/v2" + "/get-quotes?" + requiredParameters;
        return target;
    }

    /*not finished*/
    std::string MarketEndPoint::getMovers(std::string region, size_t count,
                                          std::string start,
                                          std::string lang) const {
        if (count <= 25) {
            std::string target;
            return target;
        } else {
            std::cerr << "Count must be less than 25\n";
            return "\0";
        }
    }


    StockEndPoint::StockEndPoint() : EndPoint("/stock"){};
    std::string StockEndPoint::getInsights(const std::string& ticker) const {
        std::string tempTicker{ticker};
        boost::replace_all(tempTicker, " ", spacer);
        std::string requiredParameters{"symbol"};
        std::string endPoint{"/get-insights"};
        std::string target{m_prefix + "/v2" + std::move(endPoint) + '?' + std::move(requiredParameters) + '=' + std::move(tempTicker)};
        return target;
    }
    std::string StockEndPoint::getFinancials(const std::string& ticker, const std::string& region) const {
        std::string tempTicker{ticker}, tempRegion{region};
        boost::replace_all(tempTicker, " ", spacer);
        boost::replace_all(tempRegion, " ", spacer);
        std::string requiredParameters{"symbol"};
        std::string optionalParameters{"region"};
        std::string endPoint{"/get-financials"};
        std::string target{m_prefix + "/v2" + std::move(endPoint) + '?' + std::move(requiredParameters) + '=' + std::move(tempTicker) + '&' +
                           std::move(optionalParameters) + '=' + tempRegion};
        return target;
    }

    NewsEndPoint::NewsEndPoint() : EndPoint("/news/v2"){};

    std::string NewsEndPoint::getDetails(const std::string& uuid, const std::string& region) const {
        std::string tempUuid{uuid}, tempRegion{region};
        boost::replace_all(tempUuid, " ", spacer);
        boost::replace_all(tempRegion, " ", spacer);
        std::string requiredParameters{"uuid="};
        std::string optionalParameters{"region="};
        std::string endPoint{"/get-details"};
        std::string target{m_prefix + std::move(endPoint) + '?' + std::move(requiredParameters) + std::move(tempUuid) + '&' + std::move(optionalParameters) + std::move(tempRegion)};
        return target;
    }
}// namespace yahooFinance::targets
