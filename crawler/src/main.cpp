#include <iostream>
#include <string>
#include "forms.h"
#include "crawler.h"
constexpr auto __host = "0.0.0.0";
//constexpr auto __host = "localhost";
constexpr auto __port = "5000";

//constexpr auto SettingsField = "SettingPostForm";
int main(int argc, char *argv[]) {
//    SettingPostForm in;
//    in.user_tg_id = 1;
//    in.ticker = "aapl";
//    in.period = {10, 5};
//    in.types = {0, 0, 1, 1};
//    in.summary = {25, 3};
//
//    boost::property_tree::ptree pt;
//    pt.put(SettingsField, in);
//    std::stringstream ss;
//    boost::property_tree::write_json(ss, pt);
//
//    boost::property_tree::ptree ptOut;
//    boost::property_tree::read_json(ss, ptOut);
//
//    auto out = ptOut.get<forms::settings::SettingPostForm>(SettingsField);
//    std::cout << "out=\n" << out << std::endl;
//    std::cout << "consensus=" << out.types.consensus << std::endl;
//    std::cout << "news=" << out.types.news << std::endl;


//    boost::property_tree::ptree pt;
//    forms::endPointStructs::InputData str {"aapl", "US"};
//    std::cout << str << std::endl;
//    pt.put(forms::endPointStructs::inputField, str);
//
//    boost::property_tree::write_json(std::cout, pt);
//
//    auto out = pt.get<forms::endPointStructs::InputData>(forms::endPointStructs::inputField);
//    std::cout << out << std::endl;

//    auto uniquePtr = std::make_unique<std::mutex>();
//    std::cout << uniquePtr.get();

    auto crawler = std::make_shared<crawler::CrawlerHttpServer>(__host, __port);
    crawler->run();

    return EXIT_SUCCESS;
}
