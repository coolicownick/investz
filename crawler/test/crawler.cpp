#include "crawler.h"

#include <gtest/gtest.h>

TEST(crawlerTests, testsCrawlerPublicFuncs) {
  auto tempCrawler = Singleton<Crawler>::Instance();
  auto tempSubscriber = tempCrawler->getNextSubscriber();
  EXPECT_EQ(tempSubscriber, nullptr);
  EXPECT_NO_FATAL_FAILURE(
      tempCrawler->waitSettingsFromTelegram(tempSubscriber));
  auto tempSettings = Settings();
  auto tempData = Data();
  EXPECT_NO_FATAL_FAILURE(tempCrawler->generateDataForML(tempSubscriber));
  EXPECT_NO_FATAL_FAILURE(tempCrawler->sendDataToML(tempData));
  EXPECT_NO_FATAL_FAILURE(tempCrawler->waitResponseFromML());
  EXPECT_NO_FATAL_FAILURE(tempCrawler->generateSubscriberMsg(tempSubscriber));
  auto tempMessage = Message();
  EXPECT_NO_FATAL_FAILURE(tempCrawler->pushMessageToDB(&tempMessage));
  YHFinanceAPI* tempYahoo;
  EXPECT_NO_FATAL_FAILURE(tempCrawler->findContent(tempSettings, tempYahoo));
}
