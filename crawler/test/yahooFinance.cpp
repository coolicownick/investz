#include <gtest/gtest.h>

#include "crawler.h"

TEST(yahooFinanceTests, testsYahooFinancePublicFuncs) {
  auto yahooFinance = Singleton<YHFinanceAPI>::Instance();
  HeaderParameters head;
  OptionalParameters__YF opt;
  RequiredParameters__YF req;
  EXPECT_NO_FATAL_FAILURE(yahooFinance->setParameters(head, &opt, &req));
  std::string ticker;
  EXPECT_NO_FATAL_FAILURE(yahooFinance->getNews(ticker));
  EXPECT_NO_FATAL_FAILURE(yahooFinance->getConsensus(ticker));
  EXPECT_NO_FATAL_FAILURE(yahooFinance->getPrice(ticker));
}
