#include <gtest/gtest.h>

#include "crawler.h"

TEST(alphaVantageTests, testsAlphaVantagePublicFuncs) {
  auto alphaVantage = Singleton<AlphaVantageAPI>::Instance();
  HeaderParameters head;
  OptionalParameters__AV opt;
  RequiredParameters__AV req;
  EXPECT_NO_FATAL_FAILURE(alphaVantage->setParameters(head, &opt, &req));
  std::string ticker;
  EXPECT_NO_FATAL_FAILURE(alphaVantage->getNews(ticker));
  EXPECT_NO_FATAL_FAILURE(alphaVantage->getConsensus(ticker));
  EXPECT_NO_FATAL_FAILURE(alphaVantage->getPrice(ticker));
}
