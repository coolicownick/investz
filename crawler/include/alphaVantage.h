#ifndef AV_H
#define AV_H
#include "stocksAPI.h"
#include "tools.h"

struct OptionalParameters__AV : OptionalParameters {
 public:
  std::string m_datatype;
  std::string m_output_size;
};

struct RequiredParameters__AV : RequiredParameters {
 public:
  std::string m_interval;
  std::string m_function;
  std::string m_symbol;
};

class AlphaVantageAPI : public StocksAPI {
 public:
  void setParameters(const HeaderParameters& _header,
                     const OptionalParameters__AV* _optional,
                     const RequiredParameters__AV* _required) {
    m_headerParameters = _header;
    m_optionalParameters = new OptionalParameters__AV(*_optional);
    m_requiredParameters = new RequiredParameters__AV(*_required);
  };
  void getNews(std::string& ticker) override{};
  void getConsensus(std::string& ticker) override{};
  void getPrice(std::string& ticker) override{};

 private:
  AlphaVantageAPI(){};
  ~AlphaVantageAPI(){};
  friend Singleton<AlphaVantageAPI>;
};

#endif  // AV_H
