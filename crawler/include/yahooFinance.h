#pragma once
#include <map>
#include <set>

#include "tools.h"

namespace yahooFinance {
    namespace targets {
        class EndPoint {
        protected:
            const std::string m_prefix;
        public:
            EndPoint();
            explicit EndPoint(std::string  _prefix);
            virtual ~EndPoint(){};
            std::string autoComplete(const std::string& ticker, const std::string& region);
        };

        class MarketEndPoint : EndPoint {
        public:
            MarketEndPoint();
            std::string getQuotes(std::string region, std::set<std::string>& symbols) const;
            std::string getMovers(std::string region, size_t count, std::string start,
                                  std::string lang = "en-US") const;
            std::string getSummary();
            std::string getCharts();
            std::string getSpark();
            std::string getEarnings();
            std::string getTrendingTickers();
            std::string getPopularWatchLists();
            std::string getWatchListPerfomance();
            std::string getWatchlistDetail();
        };

        class StockEndPoint : EndPoint {
        public:
            StockEndPoint();
            std::string getInsights(const std::string& ticker) const;
            std::string getFinancials(const std::string& ticker, const std::string& region) const;
        };

        class NewsEndPoint : EndPoint {
        public:
            NewsEndPoint();
//            const std::string m_prefix = "/news/v2";
            std::string getDetails(const std::string& uuid, const std::string& region) const;
        };

        class ScreenerEndPoint : EndPoint {};

        class ConversationsEndPoint : EndPoint {};
    }
    static std::set<std::string> Regions{
            "US", "BR", "AU", "CA", "FR", "DE", "HK", "IN", "IT", "ES", "GB", "SG"};

}// namespace yahooFinance
