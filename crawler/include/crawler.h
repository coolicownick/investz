#pragma once
#include "async.http.server.h"
#include "router.h"
#include "sync.http.client.h"
#include "yahooFinance.h"

namespace crawler {

    server::Response HandleGetNews(const server::Request& request);
    server::Response HandleWrite(const server::Request& request);
    server::Response HandleGetConsensus(const server::Request& request);
    server::Response HandlePrepareData(const server::Request& request);
    server::Response HandleGetPrice(const server::Request& request);

    class CrawlerHttpServer : public server::asyncHttpServer {
    public:
        explicit CrawlerHttpServer(const std::string& address, const std::string& port,
                                   std::size_t thread_pool_size = get_nprocs()) : server::asyncHttpServer(address, port, thread_pool_size) {
            request_router.addHandler("/news/last", HandleGetNews);
            request_router.addHandler("/consensus/last", HandleGetConsensus);
            request_router.addHandler("/write", HandleWrite);
            request_router.addHandler("/prepare/data", HandlePrepareData);
            request_router.addHandler("/price", HandleGetPrice);
        }
    };

    class Data {};


    class Crawler {
    private:
        //  Heap<Subscriber>* m_subscribers;
        //  Message m_generatedMessage;

        // public:
        //  const Subscriber* getNextSubscriber() const {
        //    const Subscriber* _subs = &m_subscribers->top();
        //    return _subs;
        //  }
        //  void init(Heap<Subscriber>* _data) { m_subscribers = _data; };
        //  Settings waitSettingsFromTelegram(const Subscriber*) const {
        //    return Settings();
        //  }
        //  Data generateDataForML(const Subscriber*) { return Data(); }
        //  bool sendDataToML(const Data) const { return true; }
        //  Data waitResponseFromML() const { return Data(); }
        //  Message generateSubscriberMsg(const Subscriber*) const { return Message(); }
        //  bool pushMessageToDB(Message*) const { return true; }
        //  Data findContent(Settings /*, stocksComponents::StocksAPI**/) {
        //    return Data();
        //  };

    protected:
        explicit Crawler(const Crawler&) = delete;
        explicit Crawler(Crawler&&) = delete;
        Crawler& operator=(const Crawler&) = delete;
        Crawler& operator=(Crawler&&) = delete;

    private:
        Crawler(){};
        ~Crawler(){};
        //  friend Singleton<Crawler>;
        // stocksComponents::StocksAPI* getAccessToStocksAPI();
        void findNews(){};
        void findConsensuns(){};
        void getPrice(){};
        // Struct* getSubscriberSettings(Subscriber*);
    };

}// namespace crawler
