#ifndef STOCKSAPI_H
#define STOCKSAPI_H
#include "stocksComponents.h"
struct HeaderParameters {
 public:
  std::string m_X_RapidAPI_Host;
  std::string m_X_RapidAPI_Key;

 public:
  HeaderParameters(){};
  ~HeaderParameters(){};
};

struct OptionalParameters {
 public:
  virtual ~OptionalParameters(){};

 protected:
  OptionalParameters(){};
};

struct RequiredParameters {
 public:
  virtual ~RequiredParameters(){};

 protected:
  RequiredParameters(){};
};

class StocksAPI {
 public:
  void setParameters(const HeaderParameters& _header,
                     const OptionalParameters* _optional,
                     const RequiredParameters* _required);
  virtual void getNews(std::string& ticker) = 0;
  virtual void getConsensus(std::string& ticker) = 0;
  virtual void getPrice(std::string& ticker) = 0;

 protected:
  APIes* apiInterface;
  HeaderParameters m_headerParameters;
  OptionalParameters* m_optionalParameters;
  RequiredParameters* m_requiredParameters;
};

#endif  // STOCKSAPI_H
